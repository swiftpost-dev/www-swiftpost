import Smarthubs from "./smarthubs.json";

const countSmarthubs = () => {
  return Smarthubs.locations.length;
};

export default countSmarthubs;
