import React from "react";
import { Col, Row } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { HashLink } from "react-router-hash-link";
import whiteLogo from "../../assets/footer/logo-swiftpost-white@2x.png";

import { ImTwitter, ImLinkedin } from "react-icons/im";
import { AiOutlineInstagram } from "react-icons/ai";
import { FaFacebook } from "react-icons/fa";
import { FaCanadianMapleLeaf } from "react-icons/fa";

function Footer() {
  const { i18n, t } = useTranslation();
  const [lng, setLng] = React.useState<string>("en");

  const scrollWithOffset = (el: any) => {
    const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
    const yOffset = -70;
    window.scrollTo({ top: yCoordinate + yOffset, behavior: "smooth" });
  };

  const footerNav = [
    {
      title: t("Footer.Categories.Industries.title"),
      a: [
        <HashLink to="ecommerce#top">
          {t("Footer.Categories.Industries.List.ecommerce")}
        </HashLink>,
        <HashLink to="healthcare#top">
          {t("Footer.Categories.Industries.List.healthcare")}
        </HashLink>,
        <HashLink to="beverage#top">
          {t("Footer.Categories.Industries.List.beverage")}
        </HashLink>,
      ],
    },
    {
      title: t("Footer.Categories.Company.title"),
      a: [
        <HashLink to="about-us#top">
          {t("Footer.Categories.Company.List.about")}
        </HashLink>,
        <a href="mailto:careers@swiftpost.com">
          {t("Footer.Categories.Company.List.careers")}
        </a>,
        // <HashLink to="news">
        //   {t("Footer.Categories.Company.List.news")}
        // </HashLink>,
        <HashLink to="contact#top">{t("Footer.Categories.Company.List.contactUs")}</HashLink>,
      ],
    },
    {
      title: t("Footer.Categories.Resources.title"),
      a: [
        <a
          href="javascript:void(0)" 
          onClick={() => {
          // unable to avoid using ignore here
          // @ts-ignore
          window.$zopim.livechat.window.show();
          }}
        >
          {t("Footer.Categories.Resources.List.support")}
        </a>,
        <HashLink to="/#smarthub-locator" scroll={(el) => scrollWithOffset(el)}>
          {t("Footer.Categories.Resources.List.locations")}
        </HashLink>,
        <HashLink to="tracking#top">
          {t("Footer.Categories.Resources.List.trackShipments")}
        </HashLink>,
        <HashLink to="referrals#top">
          {t("Footer.Categories.Resources.List.referFriends")}
        </HashLink>,
      ],
    },
    {
      title: t("Footer.Categories.GetStarted.title"),
      a: [
        <a
          href="https://app.swiftpost.com/signup"
          target="_blank"
          rel="noreferrer"
        >
          {t("Footer.Categories.GetStarted.List.signUp")}
        </a>,
        <a href="tel:18886556258">{t("Globals.PhoneNumber")}</a>,
        <HashLink to="contact#top">
        {t("Footer.Categories.GetStarted.List.talkToSales")}
      </HashLink>
        // <a href="">
        //   {t("Footer.Categories.GetStarted.List.talkToSales")}
        // </a>,
      ],
    },
  ];

  React.useEffect(() => {
    setLng(i18n.language);
  }, [i18n.language]);

  return (
    <footer>
      <div className="layoutContainer">
        <Row className="footerTop">
          <Col sm={12} lg={3} className="footerTopText">
            <div className="logoContainer">
              <img src="/assets/footer/logo-swiftpost-white@2x.png" alt="SwiftPost logo" />
            </div>
            <p>{t("Footer.description")}</p>
          </Col>

          <Col lg={{ offset: 1, span: 8 }} className="footerNav">
            <Row>
              {footerNav.map((item, i) => {
                return (
                  <Col className="category" sm={6} key={i}>
                    <h3>{item.title}</h3>
                    <ul>
                      {item.a.map((li, i) => {
                        return <li key={i}>{li}</li>;
                      })}
                    </ul>
                  </Col>
                );
              })}
            </Row>
          </Col>
        </Row>

        <Row sm={12}>
          <Col sm={6} md={6} className="footerSocialMedia">
            <a href="https://twitter.com/SwiftPost">
              <ImTwitter />
            </a>
            <a href="https://www.instagram.com/swiftpost/">
              <AiOutlineInstagram />
            </a>
            <a href="https://www.facebook.com/pages/category/Cargo---Freight-Company/SwiftPost-101567691399366/">
              <FaFacebook />
            </a>
            <a href="https://www.linkedin.com/company/swiftpost">
              <ImLinkedin />
            </a>
          </Col>

          <Col sm={12} md={6} className="footerTerms">
            <p>{t("Footer.Terms.description")}</p>
            <div>
              <HashLink to="privacy-policy#top">
                {t("Footer.Terms.Button.PrivacyPolicy")}
              </HashLink>
              <HashLink to="terms#top">
                {t("Footer.Terms.Button.TermsofUse")}
              </HashLink>
              {/* <a href="#">{t("Footer.Terms.Button.PrivacyPolicy")}</a>
              <a href="#">{t("Footer.Terms.Button.TermsofUse")}</a> */}
              {/* <a href={lng === "fr-CA" ? "/en-US/" : "/fr-CA/"}>
                {lng === "fr-CA" ? "English" : "Français"}
              </a> */}
            </div>
          </Col>

          <Col sm={12} className="disclaimer">
            <Row>
              <Col sm={12} md={6}>
                <p>{t("Footer.Terms.disclaimer")}</p>
              </Col>
              <Col md={6}>
                <p className="proudly">
                  {t("Footer.Terms.proudlyCanadian")} <FaCanadianMapleLeaf />
                </p>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </footer>
  );
}

export default Footer;
