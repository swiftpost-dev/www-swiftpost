import React, { useState } from "react";
import { Carousel } from "react-bootstrap";
import { useTranslation } from "react-i18next";

function CompanyCarousels() {
  const { t } = useTranslation();
  const customerReviews = [
    {
      comment: t("Company.Carousel.Reviews.Review1.description"),
      name: t("Company.Carousel.Reviews.Review1.author"),
    },
    {
      comment: t("Company.Carousel.Reviews.Review2.description"),
      name: t("Company.Carousel.Reviews.Review2.author"),
    },
    {
      comment: t("Company.Carousel.Reviews.Review3.description"),
      name: t("Company.Carousel.Reviews.Review3.author"),
    },
    {
      comment: t("Company.Carousel.Reviews.Review4.description"),
      name: t("Company.Carousel.Reviews.Review4.author"),
    },
  ];
  return (
    <div className="companyCarousels">
      <div className="layoutContainer">
        <p className="smallTitle">{t("Company.Carousel.title")}</p>
        <Carousel controls={false}>
          {customerReviews.map((review, i) => {
            return (
              <Carousel.Item>
                <Carousel.Caption>
                  <p className="comment">{review.comment}</p>
                  <p className="name smallTitle">- {review.name}</p>
                </Carousel.Caption>
              </Carousel.Item>
            );
          })}
        </Carousel>
      </div>
    </div>
  );
}
export default CompanyCarousels;
