import React from "react";
import HomeCalculator from "../homeSections/HomeCalculator";
import HomeCTA from "../homeSections/HomeCTA";
import HomeHero from "../homeSections/HomeHero";
import HomeHowItWorks from "../homeSections/HomeHowItWorks";
import HomeMap from "../homeSections/HomeMap";
import HomeSectors from "../homeSections/HomeSectors";
import HomeStats from "../homeSections/HomeStats";

function Home() {
  return (
    <div className="home">
      <HomeHero />
      <HomeHowItWorks />
      <HomeSectors />
      <HomeStats />
      <HomeMap />
      <HomeCalculator />
      <HomeCTA />
    </div>
  );
}

export default Home;
