import React from "react";
import Articles from "../templates/Articles";
import { useTranslation, Trans } from "react-i18next";

function Covid19() {
  const { t } = useTranslation();

  const content = [
    t("Covid19.Content.sanitization"),
    t("Covid19.Content.hygiene"),
    t("Covid19.Content.businessContinuity"),
    t("Covid19.Content.travel"),
  ];

  return (
    <div className="covid19">
      <Articles
        tags={[t("Covid19.tag")]}
        title={t("Covid19.title")}
        signup={false}
      >
        <div>
          <p>{t("Covid19.Content.greeting")}</p>

          <p>{t("Covid19.Content.paragraph1")}</p>

          {content.map((item, i) => {
            const strong = item.split(":", 2);
            return (
              <p>
                <strong>{strong[0]}:</strong>
                {strong[1]}
              </p>
            );
          })}

          <p>
            <Trans i18nKey="Covid19.Content.serviceCommitment">
              <strong>Service Commitment:</strong> We continue to monitor,
              evaluate and respond to this situation as it evolves, and will
              take all necessary precautions to safeguard our staff, customers
              and partners during this difficult time. This is a challenging
              situation for all of us, and we are fortunate to have the
              resources to invest in helping our staff and customers minimize
              the disruption caused by these events. We remain committed to
              providing you with the same service you have come to expect from
              us. If you have any questions or concerns about how we are
              responding to the outbreak, or how this will impact your business,
              please reach out to us at
              <a className="link" href="tel:18886556258">
                1-888-655-6258
              </a>
              or via
              <a className="link" href="mailto:support@swiftpost.com">
                support@swiftpost.com
              </a>
              .
            </Trans>
          </p>

          <p>{t("Covid19.Content.regards")}</p>

          <p>{t("Covid19.Content.author")}</p>
        </div>
      </Articles>
    </div>
  );
}

export default Covid19;
