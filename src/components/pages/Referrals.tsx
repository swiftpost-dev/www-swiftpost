import React from "react";
import HeaderSideImg from "../templates/HeaderSideImg";
import ImgOverSection from "../templates/ImgOverSection";
import { useTranslation } from "react-i18next";

function Referrals() {
  const { t } = useTranslation();
  return (
    <div className="referrals">
      <HeaderSideImg
        img="/assets/referrals/referrals-header@2x.png"
        titleText={t("Referral.title")}
      />

      {/* <ReferralsProgram /> */}
      {/* <ReferralsHow /> */}
      {/* <ReferralsFinePrint /> */}

      <ImgOverSection
        // bgColor="#BFEAF6"
        bgColor="#fff"
        img="/assets/referrals/referrals-cta-saveearn@2x.png"
        title={t("Referral.CTA.title")}
        text={t("Referral.CTA.description")}
        button={{
          text: t("Referral.CTA.button"),
          // eslint-disable-next-line no-script-url
          destination: "javascript:void(0)",
          newWindow: false,
        }}
      />
    </div>
  );
}

export default Referrals;
