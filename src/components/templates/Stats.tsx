import React from "react";
import { Col, Container, Row } from "react-bootstrap";

interface DataInterface {
  num: any;
  description: string;
}

interface StatsInterface {
  statsData: Array<DataInterface>;
}

function Stats(props: StatsInterface) {
  return (
    <div className="stats">
      <Container>
        <Row>
          {props.statsData.map((item, i) => {
            return (
              <Col key={i}>
                <h2 className="num">{item.num}</h2>
                <p className="description">{item.description}</p>
                <div className={"divider" + ` num${i}`}></div>
              </Col>
            );
          })}
        </Row>
      </Container>
    </div>
  );
}

export default Stats;
