import React from "react";
import { HashLink } from "react-router-hash-link";
import { useTranslation, Trans } from "react-i18next";

function ReferralsFinePrint() {
  const { t } = useTranslation();

  return (
    <div className="layoutContainer">
      <div className="referralsFinePrint">
        <p className="title">{t("Referral.FinePrint.title")}</p>
        <p>
          <Trans i18nKey="Referral.FinePrint.description">
            You’re credited for each referred account’s lorem ipsum dolor. Your
            referral credit will be based on the number of users who have lorem
            ipsum consectetur adipiscing elit. Referrals must be new Swiftpost
            clients who have never opened an account or shipped with us before.
            Read more about referral eligibility in our
            <HashLink to="terms#top" className="link">
              terms
            </HashLink>
            . Suspendisse congue ultrices nisi, vel pellentesque ut. There is no
            maximum limit to the amount of credits or commissions you can earn.
            <a
              href="http://support.swiftpost.com/"
              target="_blank"
              rel="noreferrer"
              className="link"
            >
              Learn more in our Support FAQ
            </a>
          </Trans>
        </p>
      </div>
    </div>
  );
}
export default ReferralsFinePrint;
