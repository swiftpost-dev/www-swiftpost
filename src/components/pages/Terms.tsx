import React from "react";
import Articles from "../templates/Articles";
import { useTranslation, Trans } from "react-i18next";

import sampleCover from "../../assets/articleOne/news_template.png";

function Terms() {
  const { t } = useTranslation();
  const sampleTags = [""];

  const acceptanceText = {
    title: t("Terms.Sections.Acceptance.title"),
    list: [
      <li>
        <Trans i18nKey="Terms.Sections.Acceptance.ontent">
          The following terms of service (the <strong>“Agreement”</strong>)
          constitute a legally binding contract between 9956433 Canada Inc.
          operating as SWIFTPOST, a Canadian corporation (
          <strong>“SWIFTPOST”, “we”, “us”,</strong> or <strong>“our”</strong>)
          and you with respect to your use of SWIFTPOST’s websites including
          www.swiftpost.com or www.swiftpost.ca (<strong>“Website”</strong>) and
          the transportation services provided through the Website or other
          software provided by SWIFTPOST (collectively, the
          <strong>“Services”</strong>). We may update the Agreement from time to
          time without notice to you. In addition, when using the Services, you
          will be subject to any guidelines or rules applicable to such Services
          that SWIFTPOST may publish or otherwise implement from time to time (
          <strong>“Rules”</strong>). The Rules are deemed incorporated by
          reference into this Agreement.
        </Trans>
      </li>,
    ],
  };

  const serviceText = {
    title: t("Terms.Sections.Service.title"),
    list: [
      <li>
        <Trans i18nKey="Terms.Sections.Service.content">
          <strong>Services.</strong> Subject to the terms of this Agreement, at
          your request from time to time, SWIFTPOST shall provide you with
          Services in connection with the transport of general commodities from
          points of origin in Canada or other countries to destinations in
          Canada or other countries.
        </Trans>
      </li>,
    ],
  };

  const feeText = {
    title: t("Terms.Sections.Fee.title"),
    list: [
      <li>
        <Trans i18nKey="Terms.Sections.Fee.Content.rates">
          <strong>Rates. </strong>Rates will be applied based on the weight and
          dimensions of each Package shipped hereunder as determined by
          SWIFTPOST using scanning technology certified by Measurement Canada.
          Absent any obvious error, you will be liable for shipping costs based
          solely on such determination, whether or not such determination
          differs from the weight and dimensions or other information that you
          provided when requesting the Services.
        </Trans>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Fee.Content.payment">
          <strong>Payment.</strong> Without limiting other rights of SWIFTPOST
          or your obligations hereunder, SWIFTPOST shall have no obligation to
          provide any Services in respect of any particular shipment until you
          have deposited into your SWIFTPOST wallet funds equal to the estimated
          cost of completing the Services in respect of such shipment. If the
          fees for a shipment exceed the amount in your SWIFTPOST wallet for
          such shipment then your package will not be shipped until you have
          deposited the balance. After completion of a shipment, any excess
          funds in your SWIFTPOST wallet can, at your request, be refunded but
          only to the same credit card or other payment account used to deposit
          such funds. If such payment account is no longer available please
          contact SWIFTPOST for more information.
        </Trans>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Fee.Content.currency">
          <strong>Currency.</strong> Rates for Canadian domiciled shippers will
          be in Canadian dollars and for United States domiciled shippers in
          United States dollars.
        </Trans>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Fee.Content.invoiceCorrection">
          <strong>Invoice Corrections.</strong> If you think there is an error
          on an invoice you must notify SWIFTPOST of such possible error within
          30 days from the invoice date, failing which such invoice is not
          subject to dispute. The notification must include the date of shipment
          and tracking number for each applicable Package.
        </Trans>
      </li>,
    ],
  };

  const terminationText = {
    title: t("Terms.Sections.Termination.title"),
    list: [
      <li>
        <Trans i18nKey="Terms.Sections.Termination.Content.termination">
          <strong>Termination.</strong> You agree that SWIFTPOST, in its sole
          discretion, may terminate your password, account or use of the
          Services, and, subject to compliance with applicable law, discard any
          information you have provided, for any reason, including, without
          limitation, for lack of use or if SWIFTPOST believes that you have
          violated or acted inconsistently with the letter or spirit of this
          Agreement. SWIFTPOST may also in its sole discretion and at any time
          discontinue providing the Services, or any part thereof, to you
          without notice. Further, you agree that SWIFTPOST will not be liable
          to you or any third-party for any termination of your access to the
          Services or expungement of your information.
        </Trans>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Termination.Content.modification">
          <strong>Modification.</strong> SWIFTPOST reserves the right at any
          time and from time to time to modify or discontinue, temporarily or
          permanently, the Services (or any part thereof) with or without
          notice. You agree that SWIFTPOST will not be liable to you or to any
          third party for any modification, suspension or discontinuance of the
          Services or your access thereto.
        </Trans>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Termination.Content.serurity">
          <strong>Account Security.</strong> You are responsible for maintaining
          the confidentiality of your password and account information, and you
          are fully responsible for all activities that occur when your password
          or account are used. You agree to immediately notify SWIFTPOST of any
          unauthorized use of your password or account or any other breach of
          security. SWIFTPOST will not be liable for any loss or damage arising
          from your failure to comply with this Agreement.
        </Trans>
      </li>,
    ],
  };

  const specificationText = {
    title: t("Terms.Sections.Specification.title"),
    list: [
      <li>
        <strong>
          {t(
            "Terms.Sections.Specification.Content.PackagingAndLabelling.description"
          )}
        </strong>
        <ol>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.PackagingAndLabelling.a">
              (a) You warrant to SWIFTPOST that, for each Package destined
              internationally, you will provide SWIFTPOST with the required
              information or paperwork to export from and import to each
              applicable jurisdiction, that the Package will be properly
              described on the commercial invoice, will be acceptable for
              transport by SWIFTPOST and its subcontractors, and will be
              properly marked, addressed, and packaged to ensure safe
              transportation with ordinary care in handling.
            </Trans>
          </li>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.PackagingAndLabelling.b">
              (b) Packages destined for final delivery to the United States or
              Canada must be accurately labelled and meet the applicable
              packaging standards specified in the then-current Canpar Express
              and also the then-current Mailing Standards of the United States
              Postal Service (Domestic Mail Manual) while those destined for
              final delivery to a jurisdiction other than Canada or the United
              States must meet the applicable standards specified in the
              then-current Mailing Standards of the United States Postal Service
              (International Mail Manual). These manuals are available on
              request from SWIFTPOST. All Packages must also be packaged so as
              to pass the tests set forth in International Safe Transit
              Association (ISTA) Procedure 3A, Procedure for Testing Packaged
              Products, published by ISTA. Finally, goods tendered to SWIFTPOST
              under this Agreement must be free from damage and the packaging
              must afford reasonable protection as determined by SWIFTPOST or
              its subcontractors in their sole judgment.
            </Trans>
          </li>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.PackagingAndLabelling.c">
              (c) SWIFTPOST does not provide special handling for Packages
              bearing “Fragile,” orientation markings (e.g., “UP” arrows), or
              any other similar markings.
            </Trans>
          </li>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.PackagingAndLabelling.d">
              (d) Final destination of a package cannot be a P.O. Box number.
              Packages require a street address including apartment/suite/unit
              number and the receiver’s telephone number. A rural route number
              is acceptable if the receiver’s full name and telephone number are
              clearly marked on all applicable Packages.
            </Trans>
          </li>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.PackagingAndLabelling.e">
              (e) SWIFTPOST will not accept any Package that weighs 70 pounds or
              more.
            </Trans>
          </li>
        </ol>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Specification.Content.ProhibitedItems.description">
          <strong>Prohibited Items.</strong> The following items, together with
          any other items identified in the Rules, are prohibited by SWIFTPOST.
          In the event that any such prohibited item enters the SWIFTPOST
          system, this does not constitute a waiver on the part of SWIFTPOST and
          SWIFTPOST shall have no liability whatsoever for loss, damage or delay
          to any such item.
        </Trans>
        <ol>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.ProhibitedItems.a">
              (a) If a Package is destined for the United States, any goods that
              are prohibited by USPS or any courier in the TFI International
              group for United States domestic shipments.
            </Trans>
          </li>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.ProhibitedItems.b">
              (b) If a Package is destined for a country other than Canada or
              the United States, any goods that are prohibited by the carrier(s)
              to whom SWIFTPOST subcontracts carriage in such other country.
            </Trans>
          </li>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.ProhibitedItems.c">
              (c) Goods that are illegal, prohibited or restricted to transport,
              export or import under any federal, state, provincial or local
              laws or regulations, in each case as applicable in any of the
              origin, transit or destination jurisdictions.
            </Trans>
          </li>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.ProhibitedItems.d">
              (d) Goods that are dangerous goods or hazardous materials that are
              subject to regulation under any statute or by any governmental
              agency in Canada, the United States or any other jurisdiction of
              origin, transit or destination.
            </Trans>
          </li>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.ProhibitedItems.e">
              (e) Goods that, in SWIFTPOST’s or its subcontractor’s judgment,
              could cause damage or delay to equipment, personnel, or other
              Packages.
            </Trans>
          </li>
          <li>
            <Trans i18nKey="Terms.Sections.Specification.Content.ProhibitedItems.f">
              (f) Food or other perishable items, liquids, animals or plants,
              glass or other fragile products, excluding alcohol permitted
              pursuant to Section 5.3 below.
            </Trans>
          </li>
        </ol>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Specification.Content.alcoholAndPrescription">
          <strong>Alcohol and Prescription Drug Delivery.</strong> If you tender
          any Package containing alcohol (which may include wine, spirits or
          beer, or any combination of them) or a prescription drug, you
          represent and warrant to SWIFTPOST and its subcontractors with respect
          to each such Package that (a) you hold, in good standing, all
          licenses, authorizations or other approvals (
          <strong>“Authorizations”</strong>) required by any governmental or
          non-governmental regulatory body having jurisdiction over the sale or
          distribution of such products (<strong>“Authorities”</strong>) and
          have the right under such Authorizations to sell and distribute such
          products to the Package recipient in accordance with the terms of this
          agreement, and (b) that the distribution of such products as
          contemplated in this agreement is in accordance with all applicable
          federal and provincial laws and with the policies and orders of
          applicable Authorities. You hereby agree to indemnify SWIFTPOST and
          its subcontractors for any damages or other liabilities they may
          suffer as a result of the inaccuracy of such representations and
          warranties. You acknowledge that SWIFTPOST and its subcontractors have
          not reviewed your Authorizations or applicable laws or policies of
          Authorities and, in any event, shall not have any responsibility for
          failure to comply with them unless you have given SWIFTPOST at least
          90 days prior written notice of any applicable requirements and
          obtained SWIFTPOST’s written agreement to comply with same. Any
          delivery of alcohol which cannot be made at the recipient’s address
          for any reason will be held at a SWIFTPOST Smarthub or CANPAR Smart
          Spot location. Any delivery of prescription drugs which cannot be made
          by the second attempt will either be delivered to a Smarthub or Smart
          Spot or, in the discretion of SWIFTPOST, may be returned to you. If
          delivered to a Smarthub or Smart Spot, the purchaser will be able to
          pick-up the Package at such location for a period of 3 days following
          the original delivery date. Notwithstanding Section 5.7, after such
          time the Package will be returned to you and you will pay to SWIFTPOST
          any additional fees charged in connection with such return.
        </Trans>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Specification.Content.refusal">
          <strong>Refusal of service.</strong> SWIFTPOST reserves the right to
          decline to provide Services to any person for any or no reason.
          Without limiting the foreoing, SWIFTPOST reserves the right to refuse
          to transport or return any Package (a) that it or its subcontractors,
          in their sole discretion, determines to be dangerous, hazardous,
          unsafe or unlawful to transport, or likely to soil, taint or otherwise
          damage other shippers’ property or SWIFTPOST’s or its subcontractors
          equipment or personnel, (b) that is improperly or insecurely packaged
          or wrapped, or is economically or operationally impracticable to
          transport to the applicable destination or (c) if believes that its
          services are being used in violation of federal, provincial, local or
          foreign law, or for fraudulent purposes, or (d) when your account is
          not in good standing.
        </Trans>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Specification.Content.damagedPackages">
          <strong>Damaged Packages.</strong> SWIFTPOST reserves the right,
          without prior notification to you, to dispose of the damaged contents
          of a Package or a damaged Package and its entire contents if, at
          SWIFTPOST’s or its subcontractors sole discretion, the contents or
          packaging may cause harm to any of their personnel, the public, or
          damage to other Packages, SWIFTPOST or its subcontractors equipment or
          facilities.
        </Trans>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Specification.Content.addressChange">
          <strong>Address Change Requests.</strong> SWIFTPOST will use
          reasonable efforts to accommodate any change the destination address
          of a Package that you make, but may not be able to redirect the
          Package prior to its delivery to the original destination address. If
          SWIFTPOST is unable to implement the address change prior to delivery
          of the Package then no additional fee will be charged to you, but in
          no event will SWIFTPOST have any liability if the Package is delivered
          to the original destination address. Any further transportation of
          such Package will be at the sole expense of you.
        </Trans>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Specification.Content.refusedPackages">
          <strong>Refused Packages.</strong> All expenses incurred by SWIFTPOST
          or its subcontractors (including, for example, transport fees, duties,
          taxes, disposal expenses or applicable surcharges) in connection with
          a Package that is refused by the consignee or for any other reason
          cannot be delivered (e.g. incorrect address) will be your
          responsibility. Subject to the foregoing, if a Package is refused,
          SWIFTPOST will endeavour to notify you promptly and give you an option
          to have the Package returned to you or destroyed. Any such return or
          disposal service charges incurred by SWIFTPOST or its subcontractors
          are payable immediately upon request. If a refused Package is not
          accepted for return by, or cannot otherwise be returned to, you
          SWIFTPOST will, subject to applicable law, retain the Package for a
          period of time in its sole discretion, but not less than 30 days.
          Following such period (a) SWIFTPOST is deemed to have fulfilled all
          obligations that it may have as a common carrier in respect of such
          Package, (b) you and consignee will be deemed to have relinquished any
          and all proprietary rights to the Package and their contents which
          remain unidentified and/or undeliverable, and (c) to the fullest
          extent permitted by law, full and clear title to such unidentified
          goods will pass to SWIFTPOST. You will be charged an additional fee
          for any such Package that is refused upon its return.
        </Trans>
      </li>,
      <li>
        <Trans i18nKey="Terms.Sections.Specification.Content.rightOfInspection">
          <strong>Right of Inspection.</strong> SWIFTPOST reserves the right to
          open and inspect any Package tendered to it for transportation, but is
          not obligated to do so.
        </Trans>
      </li>,
    ],
  };

  const customsText = {
    title: "SECTION 6 - CUSTOMS",
    list: [
      <li>
        <strong>Agent.</strong> SWIFTPOST and its subcontractors are each hereby
        appointed as agents for the performance of customs clearance for
        Packages exported from Canada or imported into the United States, where
        necessary and allowed by law. SWIFTPOST and its subcontractors are, in
        effect, nominal consignees and SWIFTPOST will designate a customs broker
        (the <strong>“Customs Broker”</strong>) as subcontractor to perform
        customs clearance. Local authorities may require documentation
        confirming that SWIFTPOST has been designated as the nominal consignee.
        You agree to comply with the terms and conditions of the agreement
        between SWIFTPOST and Customs Broker as they may be provided by
        SWIFTPOST to you from time to time.
      </li>,
      <li>
        <strong>Export Documentation.</strong> You must provide the required
        information or documentation for any required customs clearance, such as
        the commercial invoice and a certificate of origin when applicable. By
        providing this documentation, you certify to SWIFTPOST that all
        statements and information relating to exportation and importation are
        true and correct. You authorize SWIFTPOST to forward all information of
        any nature regarding Packages to any and all governmental agencies that
        require or request such information. If you do not complete all
        documents required for customs clearance of a particular Package, you
        hereby authorize and instruct SWIFTPOST, its subcontractors and the
        Customs Broker, where permitted by law, to complete, correct, or replace
        the documents at your expense. However, neither SWIFTPOST, its
        subcontractors nor the Customs Broker is obligated to do so and in no
        event shall any of them have any liability for doing so or failing to do
        so, as the case may be. If a substitute form of commercial invoice is
        needed to complete delivery of the shipment and SWIFTPOST, its
        subcontractor or the Customs Broker completes such a document, the terms
        of this Agreement will continue to govern. When Packages are held by
        customs or other agencies because of incorrect or missing documentation,
        the Packages may be determined to be undeliverable. SWIFTPOST has no
        liability for any inability to complete, or delay in, a delivery due to
        incorrect or missing documentation.
      </li>,
      <li>
        <strong>Duties, Taxes and Other Customs Charges.</strong> All export or
        import duties, taxes and other expenses, including customs penalties and
        storage charges incurred as a result of an action by a customs
        authority, or failure by you or consignee to provide proper information
        or documentation or to obtain a required license or permit will be
        charged to you. If the importer of record or consignee of a Package is
        liable for payment of applicable duties or taxes and fails to pay such
        duties or taxes, then you will pay SWIFTPOST the costs of returning such
        Package to its place of origin.
      </li>,
    ],
  };

  const privacyText = {
    title: "SECTION 7 - PRIVACY, DATA AND INTELLECTUAL PROPERTY",
    list: [
      <li>
        <strong>Privacy.</strong> You represent and warrant that you have all
        consents, in accordance with applicable laws, policies and contracts, to
        disclose to SWIFTPOST and its subcontractors and for SWIFTPOST and its
        subcontractors to use all information about identifiable individuals
        including without limitation information of a consignee or associated
        addressee in package level detail for a Package for purposes of
        SWIFTPOST and its subcontractors performing the Services contemplated in
        this Agreement, and all services reasonably incidental thereto. You
        shall retain evidence of such consent and the form of consent in an
        auditable form for at least 3 years and shall produce such records to
        SWIFTPOST upon request. You shall defend, indemnify and hold harmless
        SWIFTPOST, its subcontractors and their respective affiliates and their
        respective officers, directors, employees, agents, and their successors
        and assigns, from and against any and all liability, losses, damages,
        costs and expenses (including reasonable legal fees) of any nature
        whatsoever incurred or suffered in connection with damages arising out
        of or resulting from any breach of the warranties in this paragraph.
        Personal information that you provide to SWIFTPOST is subject to our
        Privacy Policy, the terms of which are incorporated by reference. Each
        time you use our Services, you consent to the collection, use and
        disclosure of your personal information by SWIFTPOST in accordance with
        our Privacy Policy, as amended from time to time.
      </li>,
      <li>
        <strong>Electronic Communications.</strong> You will be exchanging
        information and documents in electronic form with SWIFTPOST and its
        subcontractors. Neither SWIFTPOST nor its subcontractors shall be liable
        for any loss or damage that may be suffered by you or any of your
        customers, regardless of the cause of action, in connection with any
        data transfer, loss of data, intercept of data by third parties, failure
        to transmit, or errors in transmission of, such information or documents
        or for the effects of any computer viruses, malware, worm or other
        harmful code transmitted or losses or damages otherwise arising from
        SWIFTPOST or its subcontractors providing the services contemplated
        hereunder.
      </li>,
      <li>
        <strong>Intellectual Property.</strong> You acknowledge and agree that
        the Services and any proprietary software used in connection with the
        Services contain proprietary and confidential information that is
        protected by applicable intellectual property and other laws. You
        further acknowledge and agree information presented to you through the
        Website or otherwise in respect of the Services is protected by the law
        of copyright, trademark, service mark, patent or other proprietary
        rights laws. Except as expressly authorized by SWIFTPOST or its
        information partners, you agree not to modify, rent, lease, loan, sell,
        distribute or create derivative works based on the Website or the
        Services, in whole or in part. SWIFTPOST grants you a personal,
        non-transferable and non-exclusive right to access and use the Website
        and the Services; provided that you do not (and do not allow any third
        party to) copy, modify, create any derivative work of, reverse engineer,
        reverse assemble or otherwise attempt to discover any source code for
        the Website or the Services, sell, assign, sublicense, grant a security
        interest in or otherwise transfer any right in the Website or the
        Services or the underlying software. You agree not to obtain or attempt
        to obtain unauthorized access to the Website or the Services or the
        underlying software. You agree not to access the Website or the Services
        by any means other than through the interface that is provided by
        SWIFTPOST for use in accessing the Services.
      </li>,
    ],
  };

  const liabilityText = {
    title: "SECTION 8 - LIABILITY & INDEMNIFICATION",
    list: [
      <li>
        <strong>
          Declared Value and Maximum Liability. Subject to all the terms of this
          Agreement:
        </strong>
        <ol>
          <li>
            (a) SWIFTPOST’s liability for any Losses with respect to a Package
            shall be the lesser of your actual, direct losses and SWIFTPOST’s
            maximum liability under this Agreement.
          </li>
          <li>
            (b) SWIFTPOST’s maximum liability with regard to a Package:
            <ol>
              <li>(i) with no declared value is $100;</li>
              <li>
                (ii) with a declared value of $1 to and including $1,000 shall
                be such amount; or
              </li>
              <li>
                (iii) with a declared value in excess of $1,000 shall be $1,000.
              </li>
            </ol>
          </li>
          <li>
            (c) If you fail to pay any additional charge required for a
            particular declared value for a Package, then SWIFTPOST’s maximum
            liability with regard to such Package is $100.
          </li>
          <li>
            (d) You agree that exposure to and risk of any loss in excess of the
            amounts specified above is either assumed by you or transferred by
            you to an insurance carrier through the purchase of Shipping
            Insurance or other package insurance policy.
          </li>
        </ol>
      </li>,
      <li>
        <strong>Liability Exclusions.</strong> Without limitation to any other
        liability exclusions or limitations in this Agreement, SWIFTPOST shall
        not be liable for any Losses in connection with:
        <ol>
          <li>
            (a) any special, indirect, incidental, consequential, or punitive
            damages including, but not limited to, loss of profit or revenue
            incurred by you (or any other person or company) as a result of
            SWIFTPOST’s or its subcontractors acts or omissions, including but
            not limited to, gross negligence, negligence, failure to deliver,
            loss or theft of or damage to a Package, or late or delayed delivery
            of a Package, even if SWIFTPOST knew, should have known or was
            advised in advance of the possibility of such damages;
          </li>
          <li>
            (b) any goods that are Prohibited Items or which you are otherwise
            prohibited from shipping, which SWIFTPOST is not authorized to
            accept or which SWIFTPOST refuses to accept;
          </li>
          <li>
            (c) defects or inadequacy of the packaging you have used or for
            damage to or loss of such packaging;
          </li>
          <li>
            (d) any act or omission of any customs broker (whether chosen by
            SWIFTPOST or otherwise), the operation of any applicable law or any
            policies of the Canada Border Services Agency or U.S. Customs and
            Border Protection or other government authority;
          </li>
          <li>
            (e) the consequences of failure to deliver a Package by a stipulated
            time;
          </li>
          <li>
            (f) the act, default or omission of any person or entity (including
            any subcontractor of SWIFTPOST), other than SWIFTPOST;
          </li>
          <li>
            (g) the nature of the goods shipped, including any defect,
            characteristic or inherent vice of the shipment or any difference in
            weights of commodities caused by natural shrinkage;
          </li>
          <li>
            (h) loss of or damage to articles packed and sealed in packages by
            you or a person acting on your behalf provided
            <ol>
              <li>(i) the seal is unbroken at the time of delivery,</li>
              <li>(ii) the package retains its basic integrity, and</li>
              <li>
                (iii) receipt of shipment by the recipient without written
                notice of damage on the delivery record; or
              </li>
            </ol>
          </li>
          <li>
            (i) failure to notify you or consignee of any delay, loss or damage
            in connection with the Package or any inaccuracy in such notice.
          </li>
        </ol>
        All limitations of liability in this Agreement apply to all Packages,
        notwithstanding that you have specified a declared value for a
        particular Package and paid any additional charge.
      </li>,
      <li>
        <strong>Force Majeure.</strong> SWIFTPOST shall not be liable or
        responsible for any Losses arising due to an event or circumstance
        beyond its reasonable control, including but not limited to acts of God,
        acts of USPS or other subcontractors, natural disasters, war risks, acts
        of terrorism, nuclear damage, acts of public authorities acting with
        actual or apparent authority, acts or omissions of customs or similar
        authorities, authority of law, the application of security regulations
        imposed by the government or otherwise applicable to Packages, riots,
        strikes or other labour disputes, civil unrest, disruptions in national
        or local transportation networks, disruption or failure of communication
        and information systems, or adverse weather conditions.
      </li>,
      <li>
        <strong>Indemnities.</strong> You agree to indemnify, defend, and hold
        harmless SWIFTPOST, its subcontractors, their respective affiliates and
        their officers, directors, employees, agents from all claims, demands,
        expenses, liabilities, causes of action, enforcement procedures, and
        suits of any kind or nature brought, arising from or relating to (a) the
        contents of a Package that violates applicable law, (b) this Agreement
        or the rules or policies of subcontractors of SWIFTPOST that apply to
        the shipment of any Package hereunder, (c) your negligence or wilful
        misconduct or (d) your violation of the rights of another person.
      </li>,
      <li>
        <strong>Warranty Disclaimer.</strong> TO THE MAXIMUM EXTENT PERMITTED BY
        APPLICABLE LAW, THE SERVICES PROVIDED HEREUNDER ARE PROVIDED “AS IS” AND
        “AS AVAILABLE”, WITH ALL FAULTS AND WITHOUT REPRESENTATION, WARRANTY OR
        CONDITION OF ANY KIND, AND SWIFTPOST HEREBY DISCLAIMS ALL WARRANTIES AND
        CONDITIONS WITH RESPECT SUCH SERVICES, EITHER EXPRESS, IMPLIED, OR
        STATUTORY.
      </li>,
    ],
  };

  const insuranceText = {
    title: "SECTION 9 - INSURANCE & CLAIMS",
    list: [
      <li>
        <strong>Package Insurance.</strong>
        <ol>
          <li>
            (a) SWIFTPOST has entered into an agreement with Shipsurance
            Insurance Services, Inc. (policies underwritten by Voyager Indemnity
            Insurance Company for United States residents and by American
            Bankers Insurance Company of Florida for Canadian residents) (the{" "}
            <strong>“Shipping Insurer”</strong>) for purposes of offering
            shipping insurance for Packages (
            <strong>“Shipping Insurance”</strong>). SWIFTPOST may switch
            insurance providers as it sees fit from time to time without notice
            to you and any such replacement insurer shall (for the purpose of
            Packages insured by such replacement insurer) be the “Shipping
            Insurer”.
          </li>
          <li>
            (b) SWIFTPOST may, in its sole discretion from time to time, in
            respect of some or all Packages tendered by you, either (i) require
            that you purchase Shipping Insurance from the Shipping Insurer, (ii)
            provide the option for you to purchase Shipping Insurance from the
            Shipping Insurer; or (iii) not permit you to purchase Shipping
            Insurance from the Shipping Insurer (which, for certainty, does not
            preclude you from acquiring your own package insurance). Such
            discretion may be exercised by SWIFTPOST on any bases that it sees
            fit, including without limitation your shipping history, Package
            destination or other characteristics.
          </li>
          <li>
            (c) By purchasing Shipping Insurance you agree to comply with the
            terms and conditions of the applicable insurance policy (the{" "}
            <strong>“Insurance Policy”</strong>) and acknowledge that failure to
            do so may disentitle you to the benefit of such insurance. A copy of
            such policy is attached as an exhibit at the bottom of this
            Agreement. Shippers located in the United States are subject to
            Schedule A of the Exhibit and shippers located in Canada are subject
            to Schedule B of the Exhibit.
          </li>
        </ol>
      </li>,
      <li>
        <strong>Claims.</strong>
        <ol>
          <li>
            (a) If you have purchased Shipping Insurance for a lost or damaged
            Package you must file and administer such claim only with the
            Shipping Insurer and in accordance with requirements of the
            Insurance Policy. Upon request by you or Shipping Insurer, SWIFTPOST
            will provide to such parties information in SWIFTPOST’s possession
            relating to such claim (and without incurring any liability to you
            for providing such information), but SWIFTPOST will not have any
            further responsibilities or liabilities whatsoever to you in
            connection with the making or prosecution of any insurance claim.
          </li>
          <li>
            (b) If you did not purchase Shipping Insurance, then:
            <ol>
              <li>
                (i) a claim for loss or damage arising when a Package is in the
                possession of SWIFTPOST must be submitted to SWIFTPOST in
                writing. A claim to SWIFTPOST must be filed no later than 60
                days after the date of delivery (or expected delivery in the
                case of loss). SWIFTPOST will not be liable in respect of any
                claim not filed within such time periods, and you are deemed to
                waive its claim upon expiry of the applicable period.
              </li>
              <li>
                (ii) a claim for loss or damage arising when a Package is in the
                possession of Canpar Express, USPS or any other subcontractor of
                SWIFTPOST must be submitted by you directly to such
                subcontractor in accordance with the subcontractor’s own
                requirements. SWIFTPOST will have no responsibility for the
                filing, administration or success (or lack thereof) of any such
                claim, except that SWIFTPOST will, following written request,
                provide to you and such subcontractor information in SWIFTPOST’s
                possession relating to such claim. SWIFTPOST will incur no
                liability to you for any information it provides to such
                subcontractor.
              </li>
            </ol>
          </li>
          <li>
            (c) A claim notification to SWIFTPOST pursuant to Section 9.2(b)(i)
            must include Package details including delivery address, date of
            shipment, tracking number, a statement of what is being claimed.
            After receiving the written claim with all required information,
            SWIFTPOST will pay, decline or offer a compromise settlement in
            writing to you. In order to pay a claim or portion thereof that
            SWIFTPOST determines will be paid, SWIFTPOST requires a copy of the
            original invoice for the goods to substantiate the replacement value
            and, if applicable, evidence to prove the extent of the damage to
            the goods. If such invoice does not exist or does not show current
            replacement value then SWIFTPOST will require you to establish the
            replacement value of the goods and, if applicable, the extent of the
            damage to SWIFTPOST’s satisfaction. SWIFTPOST will not pay any claim
            or portion thereof unless all transportation charges owing by you to
            SWIFTPOST have been paid and you may not deduct the amount of any
            pending claims from any transportation charges owing to SWIFTPOST.
            SWIFTPOST shall not be liable for (and reserves the right, in its
            sole discretion, to deny claims pertaining to) a package for which
            there are no SWIFTPOST records reflecting that the package was
            tendered to SWIFTPOST or delivered into the possession of any
            subcontractor of SWIFTPOST. SWIFTPOST reserves the option to pick up
            salvage on damaged claims when the claim is paid in full.
          </li>
        </ol>
      </li>,
    ],
  };

  const generalText = {
    title: "SECTION 10 - GENERAL",
    list: [
      <li>
        <strong>Subcontractors.</strong> SWIFTPOST may engage subcontractors to
        perform the Services, any part thereof or any services incidental
        thereto without notice to or consent of you. Each subcontractor shall
        have the rights granted to SWIFTPOST under this Agreement with respect
        to Packages and the transportation thereof. No such party has authority
        to waive or vary the terms of this Agreement. References to requirements
        of subcontractors in this Agreement are references to such requirements
        as they exist from time to time and you accept sole responsibility for
        determining such requirements at the applicable time.
      </li>,
      <li>
        <strong>Entire Agreement and Conflicts.</strong> This Agreement
        constitutes the full agreement between you and SWIFTPOST relating to the
        subject matter of this Agreement and there are no other representations
        or warranties, express or implied and all prior arrangements, if any,
        are superseded. The terms of any bill of lading, shipping label or other
        transportation document issued to you are subordinate to the terms of
        this Agreement and, in the event of a conflict between any such document
        and this Agreement, the terms of this Agreement shall govern. If the
        Uniform Conditions of Carriage – General Freight (as set out in O. Reg
        643/05 to the Ontario Highway Traffic Act) or other similar regulations
        in other jurisdictions, apply to a shipment under this Agreement, to the
        maximum extent permitted by law, the provision of this Agreement shall
        govern to the extent of any conflict with such Uniform Conditions or
        similar regulations.
      </li>,
      <li>
        <strong>Headings.</strong> The inclusion of headings in this Agreement
        is for convenience only and shall not affect the construction or
        interpretation hereof.
      </li>,
      <li>
        <strong>Gender and Number.</strong> In this Agreement, unless the
        context otherwise requires, words importing the singular include the
        plural and vice versa, words importing gender include all genders or the
        neuter, and words importing the neuter include all genders.
      </li>,
      <li>
        <strong>Assignment.</strong> You may not assign your rights or
        obligations under this Agreement.
      </li>,
      <li>
        <strong>Survival.</strong> Each provision of this Agreement that (a)
        expressly survives termination, (b) by its nature does or would
        reasonably be expected to survive termination or (c) contemplates the
        performance of obligations subsequent to termination shall survive its
        termination.
      </li>,
      <li>
        <strong>Amendments; Waivers.</strong> No amendment to, or waiver of,
        this Agreement shall be binding unless it is executed in writing by the
        Party to be bound thereby. No waiver of any provision of this Agreement
        shall constitute a waiver of any other provision, nor shall any waiver
        of any provision of this Agreement constitute a continuing waiver unless
        otherwise expressly provided.
      </li>,
      <li>
        <strong>Third Party Beneficiaries.</strong> You acknowledge and agree
        that each subcontractor of SWIFTPOST is a third party beneficiary of
        this Agreement and each of them will have the right (and will be deemed
        to have accepted the right) to enforce this Agreement against you as a
        third party beneficiary thereof.
      </li>,
      <li>
        <strong>Collection Costs.</strong> In any dispute involving monies owed
        to SWIFTPOST, SWIFTPOST shall be entitled to all costs of collection,
        including legal fees and interest at 15% per annum or the highest rate
        allowed by law, whichever is less.
      </li>,
      <li>
        <strong>Inurement.</strong> This Agreement shall be binding upon, and
        inure to the benefit of, the Parties and their respective successors and
        permitted assigns.
      </li>,
      <li>
        <strong>Governing Law & Attornment.</strong> This Agreement shall be
        governed by and construed and interpreted in accordance with the laws of
        the Province of Ontario and the laws of Canada applicable therein. The
        parties hereby irrevocably attorn to the non-exclusive jurisdiction of
        the courts of Ontario with respect to any matter arising under or
        related to this Agreement.
      </li>,
      <li>
        <strong>Relationship.</strong> The relationship between you and
        SWIFTPOST under this Agreement shall be that of independent contractors.
        Nothing contained in this Agreement shall be deemed to constitute a
        relationship of agency, joint venture, partnership, landlord and tenant,
        or any relationship other than independent contractors.
      </li>,
      <li>
        <strong>Severability.</strong> The obligations of this Agreement are
        separate and divisible and in the event that any clause is deemed
        unenforceable, the balance of the Agreement shall continue in full force
        and effect.
      </li>,
      <li>
        <strong>Language.</strong> The Parties hereby confirm that it is their
        wish that this Agreement, as well as any other documents relating
        hereto, including, without limitation, all notices, be drawn up in the
        English language only. Les parties aux présentes confirment leur volonté
        que cette convention, de même que tous les documents, y compris tout
        avis, qui s’y rattachent, soient rédigés en langue anglaise.
      </li>,
    ],
  };

  const definitionText = {
    title: "SECTION 11 - DEFINITIONS",
    list: [
      <li>
        <strong>"Losses"</strong> means any claims, losses, damages, costs,
        expenses, awards, judgments, orders or other liabilities.
      </li>,
      <li>
        <strong>"Package"</strong> means any parcel or letter that is accepted
        by SWIFTPOST for delivery.
      </li>,
      <li>
        <strong>"Party"</strong> means either SWIFTPOST or you and “Parties”
        means both of them.
      </li>,
      <li>
        <strong>"USPS"</strong> means United States Postal Service, a
        subcontractor of SWIFTPOST, and its own subcontractors.
      </li>,
    ],
  };

  const contactText = {
    title: "SECTION 12 - SWIFTPOST CONTACT",
    list: [
      <li>
        {" "}
        SWIFTPOST is located at 1 - 2823 Bristol Circle, Oakville, Ontario,
        Canada and can be reached by telephone at 1-888-655-6258 or by email at
        help@swiftpost.com.
      </li>,
    ],
  };

  const termsContent = [
    acceptanceText,
    serviceText,
    feeText,
    terminationText,
    specificationText,
    customsText,
    privacyText,
    liabilityText,
    insuranceText,
    generalText,
    definitionText,
    contactText,
  ];

  return (
    <div className="terms">
      <Articles
        cover={sampleCover}
        tags={sampleTags}
        title="Terms of Use"
        signup={false}
      >
        <ol>
          {termsContent.map((term, i) => {
            return (
              <>
                <li className="sectionTitle" key={i}>
                  <h2>{term.title}</h2>
                  <ol>
                    {term.list.map((item, index) => {
                      return <>{item}</>;
                    })}
                  </ol>
                </li>
                <div className={`divider divider${i}`}></div>
              </>
            );
          })}
        </ol>
      </Articles>
    </div>
  );
}

export default Terms;
