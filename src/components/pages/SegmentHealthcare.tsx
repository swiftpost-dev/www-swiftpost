import React from "react";
import { Trans, useTranslation } from "react-i18next";
import { HashLink } from "react-router-hash-link";
import healthcareCard1 from "../../assets/segment/segment-healthcare1-card.json";
import healthcareCard2 from "../../assets/segment/segment-healthcare2-card.json";
import SegmentCTA from "../templates/SegmentCTA";
import SegmentCurvedBg from "../templates/SegmentCurvedBg";
import SegmentFourSections from "../templates/SegmentFourSections";
import SegmentTitle from "../templates/SegmentTitle";
import SegmentWhy from "../templates/SegmentWhy";

function SegmentHealthcare() {
  const { t } = useTranslation();
  const segmentWhyData = [
    {
      num: (
        <Trans
          i18nKey="Healthcare.WhySwiftpost.Stat.DeliveryPercentage.num"
          values={{ deliveryPercentage: "98.5%" }}
        />
      ),
      description: t(
        "Healthcare.WhySwiftpost.Stat.DeliveryPercentage.description"
      ),
    },
    {
      num: (
        <Trans
          i18nKey="Healthcare.WhySwiftpost.Stat.BusinessNum.num"
          values={{ businessNum: "1000+" }}
        />
      ),
      description: t("Healthcare.WhySwiftpost.Stat.BusinessNum.description"),
    },
    {
      num: (
        <Trans
          i18nKey="Healthcare.WhySwiftpost.Stat.PostalCount.num"
          values={{ postalCount: "900K" }}
        />
      ),
      description: t("Healthcare.WhySwiftpost.Stat.PostalCount.description"),
    },
  ];

  const segmentWhyBottomText =
    "Retail pharmacies and dental laboratories trust Swiftpost with their " +
    (
      <span className="yellow">
        'time-sensitive, mission critical deliveries. '
      </span>
    );

  const segmentWhyImages = [
    {
      url: "/assets/segment/mckesson-canada-white.png",
      alt: t("Healthcare.WhySwiftpost.Partner.ImageAlt.mckessan"),
    },
    {
      url: "/assets/segment/krestlab.png",
      alt: t("Healthcare.WhySwiftpost.Partner.ImageAlt.krest"),
    },
    // {
    //   url: "/assets/segment/aurumLogo@2x.png",
    //   alt: t("Healthcare.WhySwiftpost.Partner.ImageAlt.aurum"),
    // },
    // {
    //   url: "/assets/segment/orthodentlogo-white-resized.png",
    //   alt: t("Healthcare.WhySwiftpost.Partner.ImageAlt.orthodent"),
    // },
    // {
    //   url: "/assets/segment/Image_Dental_White-resized.png",
    //   alt: t("Healthcare.WhySwiftpost.Partner.ImageAlt.imageDental"),
    // },
    {
      url: "/assets/segment/guardianIDALogo.png",
      alt: t("Healthcare.WhySwiftpost.Partner.ImageAlt.guradian"),
    },
  ];

  const advantagesText = t("Healthcare.Quote.List.description");

  return (
    <div className="segmentHealthcare">
      <SegmentTitle
        img="/assets/segment/segment-healthcare-landing-video-firstframe.jpg"
        video="/assets/segment/segment-healthcare-landing-video.mp4"
        title={t("Healthcare.Header.title")}
        description={t("Healthcare.Header.description")}
        color="#FFF"
      />
      <SegmentCurvedBg
        img="/assets/segment/segment-healthcare-bg-curve-bottom-cyan25@2x.png"
        bgcolor="#E5F7FB"
        color="#051e51"
      >
        <p className="description">
          <Trans i18nKey="Healthcare.CurvedBg.description">
            Grow your business with fast & dependable
            <span className="underlinedText">Rx delivery</span>
          </Trans>
        </p>
      </SegmentCurvedBg>
      <SegmentFourSections
        reduceImg="/assets/segment/segment-healthcare1-main.jpg"
        reduceImgAlt={t("Healthcare.FourSections.Reduce.imageAlt")}
        reduceCard={healthcareCard1}
        reduceTitle={t("Healthcare.FourSections.Reduce.title")}
        reduceContent={t("Healthcare.FourSections.Reduce.description")}
        simpleImg="/assets/segment/segment-healthcare2-main.jpg"
        simpleImgAlt={t("Healthcare.FourSections.Simple.imageAlt")}
        simpleCard={healthcareCard2}
        simpleTitle={t("Healthcare.FourSections.Simple.title")}
        simpleContent={t("Healthcare.FourSections.Simple.description")}
        saveContent={t("Healthcare.FourSections.Save.description")}
        contactImg="/assets/segment/segment-healthcare-cta-contactsales@2x.png"
        contactImgAlt={t("Healthcare.FourSections.Contact.imageAlt")}
      />
      <div className="healthcareQuote">
        <div className="smImg">
          <img
            src="/assets/segment/segment-healthcare3-small.jpg"
            alt={t("Healthcare.Quote.imgAlt")}
          />
        </div>

        <div className="mdImg">
          <img
            src="/assets/segment/segment-healthcare3-largemedium.jpg"
            alt={t("Healthcare.Quote.imgAlt")}
          />
        </div>
        <div className="quoteBottom">
          <div className="lgLeft">
            <p className="subtitle">{t("Healthcare.Quote.title")}</p>

            <HashLink to="contact#top">
              <button className="bttn outlinedBttn lg">
                {t("Healthcare.Quote.button")}
              </button>
            </HashLink>
          </div>

          <div className="quoteContent">
            <p>{t("Healthcare.Quote.description")}</p>

            <p className="title">{t("Healthcare.Quote.List.title")}</p>

            <ul>
              {advantagesText.split("; ").map((item) => {
                return (
                  <li>
                    <span>{item}</span>
                  </li>
                );
              })}
            </ul>
          </div>

          <HashLink to="contact#top">
            <button className="bttn outlinedBttn md">
              {t("Healthcare.Quote.button")}
            </button>
          </HashLink>
        </div>
      </div>

      <SegmentWhy statsData={segmentWhyData} images={segmentWhyImages}>
        <p className="text">
          <Trans i18nKey="Healthcare.WhySwiftpost.Partner.description">
            Healthcare providers trust Swiftpost with their
            <span className="yellowText">
              time-sensitive, mission critical deliveries.
            </span>
          </Trans>
        </p>
      </SegmentWhy>
      <SegmentCTA illustration="/assets/segment/segment-healthcare-cta-illustration@2x.png" />
    </div>
  );
}

export default SegmentHealthcare;
