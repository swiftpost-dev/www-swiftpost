const path = require("path");
const fs = require("fs");
const cracoBabelLoader = require("craco-babel-loader");

// manage relative paths to packages
const appDirectory = fs.realpathSync(process.cwd());
const resolvePackage = (relativePath) =>
  path.resolve(appDirectory, relativePath);

const modules = ["@react-leaflet", "react-leaflet"];

console.log("Transpiling packages to ES5", modules.join(""));

module.exports = {
  plugins: [
    {
      plugin: cracoBabelLoader,
      options: {
        includes: modules.map((module) =>
          resolvePackage(`node_modules/${module}`)
        ),
      },
    },
  ],
};
