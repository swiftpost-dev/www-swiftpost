import React from "react";
import OnTopToSide from "./OnTopToSide";
import { useTranslation } from "react-i18next";
import { HashLink } from "react-router-hash-link";

interface SegmentCTAInterface {
  illustration: string;
}

function SegmentCTA(props: SegmentCTAInterface) {
  const { t } = useTranslation();
  return (
    <div className="segmentCTA">
      <div className="layoutContainer">
        <div className="contact">
          <div className="contactImg">
            <img src="/assets/segment/segment-cta-shipmoreforless@2x.png" alt="" />
          </div>
          <div className="contactText">
            <p>{t("SegmentsCommon.CTA.RequestInfo.description")}</p>
            <HashLink to="contact#top">
              {t("SegmentsCommon.CTA.RequestInfo.button")}
            </HashLink>
          </div>
        </div>

        <OnTopToSide
          titleText={t("SegmentsCommon.CTA.GetStarted.title")}
          img={props.illustration}
          buttonText={t("SegmentsCommon.CTA.GetStarted.button")}
        />
      </div>
    </div>
  );
}

export default SegmentCTA;
