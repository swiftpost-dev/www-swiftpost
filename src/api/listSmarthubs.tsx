import Smarthubs from "./smarthubs.json";

const listSmarthubs = () => {
  return Smarthubs.locations;
};

export default listSmarthubs;
