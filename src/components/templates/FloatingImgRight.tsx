import React from "react";
import SlideAnimate from "./SlideAnimate";

interface FloatingImgRightInterface {
  imgOnTop: string;
  imgBottom: string;
}

function FloatingImgRight(props: FloatingImgRightInterface) {
  return (
    <div className="floatingContainer right">
      <SlideAnimate>
        <div className="onBottom floatingSub">
          <img src={props.imgBottom} alt="" />
        </div>
        <div className="onTop floatingSub">
          <img src={props.imgOnTop} alt="" />
        </div>
      </SlideAnimate>
    </div>
  );
}

export default FloatingImgRight;
