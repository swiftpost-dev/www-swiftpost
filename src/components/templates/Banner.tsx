import React, { useState } from "react";
import { useTranslation, Trans } from "react-i18next";
import { HashLink } from "react-router-hash-link";
import { IoIosArrowForward, IoMdClose } from "react-icons/io";

function Banner() {
  const { t } = useTranslation();
  const [isClose, setIsClose] = useState(false);

  const closeBanner = () => {
    setIsClose(true);
  };

  return (
    <div className="bannerOuter">
      {isClose ? null : (
        <div className="banner">
          <div className="layoutContainer">
            <p>
              {t("Banner.description")}
              <HashLink to="covid-19#top" className="yellowText">
                <Trans i18nKey="Banner.button">
                  Learn More
                  <IoIosArrowForward />
                </Trans>
              </HashLink>
            </p>

            <a className="closeBanner" onClick={closeBanner}>
              <IoMdClose />
            </a>
          </div>
        </div>
      )}
    </div>
  );
}

export default Banner;
