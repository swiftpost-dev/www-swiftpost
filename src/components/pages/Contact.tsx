import { Formik } from "formik";
import React, { useState } from "react";
import { Alert, Form, FormControl } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { AiFillCheckCircle } from "react-icons/ai";
import { FiAlertTriangle } from "react-icons/fi";
import { ImSpinner8 } from "react-icons/im";
import { useHistory } from "react-router-dom";
import * as Yup from "yup";
import ContactSales from "../../api/contactSales";

type DestinationCountry = "Canada" | "US" | "International";

type ShippingVolume = {
  [key: string]: any;
};

interface FormValues {
  name: string;
  email: string;
  phoneNumber: string;
  businessName: string;
  destinationCountry: Array<DestinationCountry>;
  shippingVolume: ShippingVolume;
  packageWeight: string;
  industry: string;
  message: string;
}

function Contact() {
  const { t } = useTranslation();
  const history = useHistory();

  const [submissionSuccess, setSubmissionSuccess] = useState(false);
  const [submissionError, setSubmissionError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const scrollToTop = () => {
    window.scrollTo({ top: 170, behavior: "smooth" });
  };

  const initialValues: FormValues = {
    name: "",
    email: "",
    phoneNumber: "",
    businessName: "",
    destinationCountry: [],
    shippingVolume: {
      Canada: 1,
      US: 1,
      International: 1,
    },
    packageWeight: "<1lb",
    industry: "Healthcare",
    message: "",
  };

  const inputFields: Array<
    "name" | "email" | "phoneNumber" | "businessName"
  > = ["name", "email", "phoneNumber", "businessName"];

  const countries: Array<DestinationCountry> = [
    "US",
    "Canada",
    "International",
  ];

  const numberRegex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/;

  const formSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, t("Contact.FormContent.InputForm.Error.Name.short"))
      .required(t("Contact.FormContent.InputForm.Error.Name.required")),
    email: Yup.string()
      .email(t("Contact.FormContent.InputForm.Error.Email.format"))
      .required(t("Contact.FormContent.InputForm.Error.Email.required")),
    phoneNumber: Yup.string()
      .required(t("Contact.FormContent.InputForm.Error.phoneNumber.required"))
      .matches(
        numberRegex,
        t("Contact.FormContent.InputForm.Error.phoneNumber.format")
      ),
    businessName: Yup.string().min(
      2,
      t("Contact.FormContent.InputForm.Error.businessName")
    ),
    destinationCountry: Yup.array().min(
      1,
      t("Contact.FormContent.InputForm.Error.destinationCountry")
    ),
    shippingVolume: Yup.object().shape({
      Canada: Yup.number().when("destinationCountry", {
        is: (val: Array<DestinationCountry>) =>
          val && val.find((element) => element == "Canada"),
        then: Yup.number().min(
          1,
          t("Contact.FormContent.InputForm.Error.shippingVolume")
        ),
      }),
      US: Yup.number().when("destinationCountry", {
        is: (val: Array<DestinationCountry>) =>
          val && val.find((element) => element == "US"),
        then: Yup.number().min(
          1,
          t("Contact.FormContent.InputForm.Error.shippingVolume")
        ),
      }),
      International: Yup.number().when("destinationCountry", {
        is: (val: Array<DestinationCountry>) =>
          val && val.find((element) => element == "International"),
        then: Yup.number().min(
          1,
          t("Contact.FormContent.InputForm.Error.shippingVolume")
        ),
      }),
    }),
    message: Yup.string().max(
      1000,
      t("Contact.FormContent.InputForm.Error.message")
    ),
  });

  const handleClose = () => {
    setSubmissionError("");
  };

  return (
    <div className="contact" id="contact">
      {submissionError ? (
        <Alert variant="danger" dismissible onClose={handleClose}>
          <div className="layoutContainer">{submissionError}</div>
        </Alert>
      ) : null}
      <div className="articles">
        <div className="articlesTitle">
          <div className="articlesBg">
            <div className="titleText">
              <div className="layoutContainer">
                <h1>{t("Contact.title")}</h1>
              </div>
            </div>

            <div className="curvedBg">
              <img
                src="/assets/articleOne/news-bg-curve-bottom-cyan25@2x.png"
                alt=""
              />
            </div>
          </div>
        </div>

        <div className="articleContent">
          <div className="layoutContainer">
            <div className="articleMain formContainer">
              <div className="topBar">
                {isLoading ? null : (
                  <p className="title">
                    {t("Contact.FormContent.InputForm.title")}
                  </p>
                )}
              </div>

              <Formik
                onSubmit={async (values, { setSubmitting }) => {
                  setIsLoading(true);
                  setSubmitting(true);
                  await ContactSales(values)
                    .then((e) => {
                      // @ts-ignore
                      window.$zopim.livechat.setName(values.name);
                      // @ts-ignore
                      window.$zopim.livechat.setEmail(values.email);
                      setSubmitting(false);
                      setIsLoading(false);
                      setSubmissionSuccess(true);
                    })
                    .catch((e) => {
                      setSubmitting(false);
                      setIsLoading(false);
                      setSubmissionError(t("Contact.Submission.error"));
                    });
                  scrollToTop();
                }}
                initialValues={initialValues}
                validationSchema={formSchema}
              >
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  setFieldValue,
                }) => (
                  <Form
                    onSubmit={handleSubmit}
                    className={isLoading ? "loading" : undefined}
                  >
                    <div className="loader">
                      <ImSpinner8 />
                    </div>
                    {submissionSuccess ? (
                      <div className="successed">
                        <div className="iconContainer">
                          <AiFillCheckCircle />
                        </div>
                        <h3>{t("Contact.FormContent.Success.title")}</h3>
                        <p>{t("Contact.FormContent.Success.subtitle")}</p>
                      </div>
                    ) : (
                      <>
                        {" "}
                        {inputFields.map((item, i) => {
                          return (
                            <Form.Group controlId={item} key={item}>
                              <Form.Label>
                                {t(
                                  `Contact.FormContent.InputForm.Labels.${item}`
                                )}
                              </Form.Label>
                              <FormControl
                                onChange={handleChange}
                                onBlur={handleBlur}
                                name={item}
                                value={values[item]}
                              />
                              {errors[item] && touched[item] ? (
                                <div className="error">
                                  <FiAlertTriangle color="#BA0007" />{" "}
                                  <p>{errors[item]}</p>
                                </div>
                              ) : null}
                            </Form.Group>
                          );
                        })}
                        <Form.Group
                          controlId="destinationCountry"
                          className="destinationCountry"
                        >
                          <Form.Label>
                            {t(
                              "Contact.FormContent.InputForm.Labels.Destination.description"
                            )}
                          </Form.Label>
                          {[
                            t(
                              "Contact.FormContent.InputForm.Labels.Destination.us"
                            ),
                            t(
                              "Contact.FormContent.InputForm.Labels.Destination.ca"
                            ),
                            t(
                              "Contact.FormContent.InputForm.Labels.Destination.international"
                            ),
                          ].map((type: string) => (
                            <div key={type}>
                              <Form.Group>
                                <Form.Check
                                  type="checkbox"
                                  id={type}
                                  label={type}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={type}
                                  name="destinationCountry"
                                />

                                <Form.Group
                                  controlId="dailyVolume"
                                  className={
                                    values.destinationCountry.find(
                                      (item) => item === type
                                    )
                                      ? "dailyVolume"
                                      : "hidden"
                                  }
                                >
                                  <Form.Label>
                                    {t(
                                      "Contact.FormContent.InputForm.Labels.Destination.shippingVolume"
                                    )}
                                    {type}
                                  </Form.Label>
                                  <FormControl
                                    name="shippingVolume"
                                    type="number"
                                    min="1"
                                    onChange={(e) => {
                                      let tobeSet: ShippingVolume =
                                        values.shippingVolume;
                                      tobeSet[type] = Number(
                                        e.currentTarget.value
                                      );
                                      setFieldValue("shippingVolume", tobeSet);
                                    }}
                                    value={values["shippingVolume"][type]}
                                  />
                                  {errors["shippingVolume"] &&
                                  touched["shippingVolume"] ? (
                                    <div className="error">
                                      <FiAlertTriangle color="#BA0007" />
                                      <p>{errors["shippingVolume"]}</p>
                                    </div>
                                  ) : null}
                                </Form.Group>
                              </Form.Group>
                            </div>
                          ))}
                          {errors["destinationCountry"] &&
                          touched["destinationCountry"] ? (
                            <div className="error">
                              <FiAlertTriangle color="#BA0007" />
                              <p>{errors["destinationCountry"]}</p>
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group controlId="packageWeight">
                          <Form.Label>
                            {t(
                              "Contact.FormContent.InputForm.Labels.PackageWeight.title"
                            )}
                          </Form.Label>
                          <Form.Control
                            as="select"
                            name="packageWeight"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.packageWeight}
                          >
                            <option value="<1">
                              {t(
                                "Contact.FormContent.InputForm.Labels.PackageWeight.lessThanOnePound"
                              )}
                            </option>
                            <option value=">=1">
                              {t(
                                "Contact.FormContent.InputForm.Labels.PackageWeight.onePoundOrMore"
                              )}
                            </option>
                          </Form.Control>
                        </Form.Group>
                        <Form.Group controlId="industry">
                          <Form.Label>
                            {t(
                              "Contact.FormContent.InputForm.Labels.Industry.description"
                            )}
                          </Form.Label>
                          <Form.Control
                            as="select"
                            name="industry"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.industry}
                          >
                            {[
                              t(
                                "Contact.FormContent.InputForm.Labels.Industry.healthcare"
                              ),
                              t(
                                "Contact.FormContent.InputForm.Labels.Industry.ecommerce"
                              ),
                              t(
                                "Contact.FormContent.InputForm.Labels.Industry.beverage"
                              ),
                            ].map((type) => (
                              <option key={type} value={type}>
                                {t(
                                  `Contact.FormContent.InputForm.Labels.Industry.${type.toLowerCase()}`
                                )}
                              </option>
                            ))}
                          </Form.Control>
                        </Form.Group>
                        <Form.Group controlId="message">
                          <Form.Label>
                            {t("Contact.FormContent.InputForm.Labels.message")}
                          </Form.Label>

                          <Form.Control
                            as="textarea"
                            name="message"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />

                          {errors["message"] && touched["message"] ? (
                            <div className="error">
                              <FiAlertTriangle color="#BA0007" />
                              <p>{errors["message"]}</p>
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group></Form.Group>
                        <Form.Group className="submitButton">
                          <button
                            type="submit"
                            className="bttn primaryBttn"
                            disabled={isSubmitting}
                          >
                            {t("Contact.button")}
                          </button>
                        </Form.Group>
                      </>
                    )}
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Contact;
