import React, { useState } from "react";
import {
  Col,
  Form,
  FormControl,
  InputGroup,
  Row,
  Spinner,
} from "react-bootstrap";
import { Trans, useTranslation } from "react-i18next";
import { BsArrowRight } from "react-icons/bs";
import { FiAlertTriangle, FiRefreshCw } from "react-icons/fi";
import request from "request";

type PostalCheckObject = {
  status: boolean;
  city: string;
  state: string;
  postal: string;
};

type ChargeDetailType = {
  title: string;
  amount: string;
};

type PriceObject = {
  total: string;
  currency: string;
  charge_detail: Array<ChargeDetailType>;
};

type originPostalListInterface = {
  [key: string]: string;
};

function Calculator() {
  const { t } = useTranslation();
  const [itemName, setItemName] = useState<string>(
    t("Home.Calculator.FormContent.InputForm.Fields.Item.Options.jewelry")
  );
  const [weight, setWeight] = useState<string>("0.1");
  const [origin, setOrigin] = useState<any>("Toronto,Ontario");
  const [destinationCountry, setDestinationCountry] = useState<string>("US");
  const [destinationPostal, setDestinationPostal] = useState<string>("");
  const [postalError, setPostalError] = useState<boolean>(false);
  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState<PriceObject | undefined>(undefined);

  const itemsList = [
    {
      name: t(
        "Home.Calculator.FormContent.InputForm.Fields.Item.Options.jewelry"
      ),
      weight: 0.1,
    },
    {
      name: t(
        "Home.Calculator.FormContent.InputForm.Fields.Item.Options.tshirt"
      ),
      weight: 0.25,
    },
    {
      name: t(
        "Home.Calculator.FormContent.InputForm.Fields.Item.Options.dress"
      ),
      weight: 0.5,
    },
    {
      name: t(
        "Home.Calculator.FormContent.InputForm.Fields.Item.Options.purse"
      ),
      weight: 1,
    },
    {
      name: t(
        "Home.Calculator.FormContent.InputForm.Fields.Item.Options.shoes"
      ),
      weight: 2,
    },
    {
      name: t(
        "Home.Calculator.FormContent.InputForm.Fields.Item.Options.subscriptionBox"
      ),
      weight: 5,
    },
    {
      name: t(
        "Home.Calculator.FormContent.InputForm.Fields.Item.Options.textbooks"
      ),
      weight: 10,
    },
  ];

  const originList = [
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.toronto"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.on"
      ),
      provinceShortForm: "ON",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.ottawa"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.on"
      ),
      provinceShortForm: "ON",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.montreal"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.qc"
      ),
      provinceShortForm: "QC",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.quebecCity"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.qc"
      ),
      provinceShortForm: "QC",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.vancouver"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.bc"
      ),
      provinceShortForm: "BC",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.victoria"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.bc"
      ),
      provinceShortForm: "BC",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.calgary"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.ab"
      ),
      provinceShortForm: "AB",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.edmonton"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.ab"
      ),
      provinceShortForm: "AB",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.winnipeg"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.mb"
      ),
      provinceShortForm: "MB",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.halifax"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.ns"
      ),
      provinceShortForm: "MB",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.capeBreton"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.ns"
      ),
      provinceShortForm: "NS",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.regina"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.sk"
      ),
      provinceShortForm: "SK",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.saskatoon"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.sk"
      ),
      provinceShortForm: "SK",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.moncton"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.nb"
      ),
      provinceShortForm: "NB",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.saintJohn"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.nb"
      ),
      provinceShortForm: "NB",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.stJohns"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.nl"
      ),
      provinceShortForm: "NL",
    },
    {
      city: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.City.charlottetown"
      ),
      province: t(
        "Home.Calculator.FormContent.InputForm.Fields.Origin.Province.pe"
      ),
      provinceShortForm: "PE",
    },
  ];

  const originPostalList: originPostalListInterface = {
    Toronto: "M6C3R6",
    Ottawa: "K1A0B1",
    Montreal: "H1A0A1",
    "Quebec City": "G1A1C5",
    Vancouver: "V5H3Z7",
    Victoria: "V8P1A1",
    Calgary: "T1Y1A1",
    Edmonton: "T5A0A7",
    Winnipeg: "R2C0A1",
    Halifax: "B0J1N0",
    "Cape Breton": "B1M1B8",
    Regina: "S0G3W0",
    Saskatoon: "S7H0A4",
    Moncton: "E1A0A6",
    "Saint John": "E2H0A4",
    "St.John's": "A1A0A4",
    Charlottetown: "C1A0A4",
  };

  const scrollWithOffset = (el: any) => {
    const yCoordinate = document.querySelector(el).getBoundingClientRect().top + window.pageYOffset;
    const yOffset = -70;
    window.scrollTo({ top: yCoordinate + yOffset, behavior: "smooth" });
  };

  const isCAPostal = (str: string) => {
    return /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/.test(str);
  };

  const isUSPostal = (str: string) => {
    return /^[0-9]{5}(?:-[0-9]{4})?$/.test(str);
  };

  const postalVerify = async (
    postal: string,
    targetCountry: string
  ): Promise<PostalCheckObject> => {
    return new Promise((resolve, reject) => {
      const body = {
        api_key: "SP_TEST",
        country: targetCountry,
        postal: postal,
      };
      request(
        {
          url: "https://enterprise-sandbox-api.swiftpost.com/verifyAddress",
          method: "POST",
          headers: {
            "content-type": "application/json",
          },
          body: body,
          json: true,
        },
        function (e, r, b) {
          if (r.statusCode !== 200) {
            reject(r.statusCode);
            setPostalError(true);
            return setLoading(false);
          }
          resolve(b);
        }
      );
    });
  };

  const removeWhiteSpace = (str: string) => {
    return str.split(" ").join("");
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    setLoading(true);
    if (
      !destinationPostal ||
      (!isCAPostal(destinationPostal) && destinationCountry === "CA") ||
      (!isUSPostal(destinationPostal) && destinationCountry === "US")
    ) {
      setPostalError(true);
      return setLoading(false);
    }
    scrollWithOffset('#resultTop')

    const verifyResult = await postalVerify(
      destinationPostal,
      destinationCountry
    );
    if (!verifyResult.status) {
      setPostalError(true);
      return setLoading(false);
    }

    const splitedOrigin = origin.split(",");

    const originPostal = originPostalList[splitedOrigin[0]];

    const qs: { [key: string]: any } = {
      m: `10,6,4,${weight}`,
      r: `${removeWhiteSpace(verifyResult.city)},${removeWhiteSpace(
        verifyResult.state
      )},${removeWhiteSpace(verifyResult.postal)},${destinationCountry}`,
      s: `${origin},${originPostal},CA`,
    };
    // const qs: { [key: string]: any } = {
    //   m: `10,6,4,${weight}`,
    //   r: `${removeWhiteSpace(verifyResult.city)},${removeWhiteSpace(verifyResult.state)},${removeWhiteSpace(verifyResult.postal)},us`,
    //   s: `${origin},${originPostal},CA`,
    // };

    if (itemName === "Textbooks") {
      qs["t"] = "MM";
    }

    request(
      {
        url: "https://www.swiftpost.com/rate",
        method: "GET",
        qs: qs,
      },
      function (e, r, b) {
        if (r?.statusCode !== 200 ?? true) {
          setLoading(false);
          return setPostalError(true);
        }
        let modifiedResult = JSON.parse(b);
        modifiedResult.total = Number(modifiedResult["total"]).toFixed(2);
        modifiedResult.charge_detail.map((item: ChargeDetailType) => {
          item["amount"] = Number(item["amount"]).toFixed(2);
        });
        setResult(modifiedResult);
        setLoading(false);
      }
    );
  };

  const handleRefresh = () => {
    setResult(undefined);
    setWeight("0.1");
    setItemName("Jewelry");
    setOrigin("Toronto,Ontario");
    setDestinationCountry("US");
    setDestinationPostal("");
    setPostalError(false);
  };

  return (
    <div className="calculatorContainer" id="resultTop">
      <div
        className={
          result ? "result contentWrapper" : "calculator contentWrapper"
        }
      >
        {!result ? (
          <div className="topBar">
            <p className="title">
              {t("Home.Calculator.FormContent.InputForm.title")}
            </p>
            <p className="tag">
              {t("Home.Calculator.FormContent.InputForm.tag")}
            </p>
          </div>
        ) : (
          <div className="topBar">
            <p className="title">
              <Trans
                i18nKey="Home.Calculator.FormContent.Result.title"
                values={{ weight: weight }}
              />
            </p>
            <div className="locations">
              <p>{origin.split(",")[0]}</p>
              <div className="iconContainer yellowText">
                <BsArrowRight />
              </div>
              <p>{destinationPostal.toUpperCase()}</p>
            </div>
          </div>
        )}

        <div className={loading ? "lowerContainer loading" : "lowerContainer"}>
          {loading ? (
            <div className="loaderContainer">
              <Spinner animation="border" />
            </div>
          ) : (
            <div>
              {!result ? (
                <Form onSubmit={handleSubmit}>
                  <Form.Group controlId="itemName">
                    <Form.Label>
                      {t(
                        "Home.Calculator.FormContent.InputForm.Fields.Item.label"
                      )}
                    </Form.Label>
                    <Form.Control
                      as="select"
                      name="weight"
                      onChange={(e) => {
                        const targetValue = e.currentTarget.value.split(",");
                        setWeight(targetValue[1]);
                        setItemName(targetValue[0]);
                      }}
                    >
                      {itemsList.map((item, i) => {
                        return (
                          <option
                            key={i}
                            value={[item.name, item.weight.toString()]}
                          >
                            {item.name} ({item.weight}{" "}
                            {item.weight === 1 ? "lb" : "lbs"})
                          </option>
                        );
                      })}
                    </Form.Control>
                  </Form.Group>

                  <Form.Group controlId="origin">
                    <Form.Label>
                      {t(
                        "Home.Calculator.FormContent.InputForm.Fields.Origin.label"
                      )}
                    </Form.Label>
                    <Form.Control
                      as="select"
                      className="caOption"
                      name="origin"
                      onChange={(e) => setOrigin(e.currentTarget.value)}
                    >
                      {originList.map((location, i) => {
                        return (
                          <option
                            key={i}
                            value={[location.city, location.provinceShortForm]}
                          >
                            {location.city}, {location.province}
                          </option>
                        );
                      })}
                    </Form.Control>
                  </Form.Group>

                  <Form.Group controlId="destination" className="destination">
                    <Form.Label>
                      {t(
                        "Home.Calculator.FormContent.InputForm.Fields.Destination.label"
                      )}
                    </Form.Label>
                    <Form.Control
                      as="select"
                      name="destinationCountry"
                      value={destinationCountry}
                      className={
                        destinationCountry === "US" ? "usOption" : "caOption"
                      }
                      onChange={(e) =>
                        setDestinationCountry(e.currentTarget.value)
                      }
                    >
                      <option value="US">
                        {t(
                          "Home.Calculator.FormContent.InputForm.Fields.Destination.Country.us"
                        )}
                      </option>
                      <option value="CA">
                        {t(
                          "Home.Calculator.FormContent.InputForm.Fields.Destination.Country.ca"
                        )}
                      </option>
                    </Form.Control>
                  </Form.Group>

                  <Form.Group controlId="postal" className="postal">
                    <FormControl
                      name="destinationPostal"
                      value={destinationPostal}
                      onChange={(e) => {
                        setDestinationPostal(e.currentTarget.value.trim());
                        setPostalError(false);
                      }}
                      isInvalid={postalError}
                      placeholder={t(
                        "Home.Calculator.FormContent.InputForm.Fields.Destination.placeHolder"
                      )}
                      aria-label={t(
                        "Home.Calculator.FormContent.InputForm.Fields.Destination.placeHolder"
                      )}
                    />
                    <InputGroup.Append className="alert">
                      <FiAlertTriangle color="#BA0007" />
                    </InputGroup.Append>
                  </Form.Group>

                  <button type="submit" className="bttn primaryBttn">
                    {t("Home.Calculator.FormContent.InputForm.button")}
                  </button>
                </Form>
              ) : (
                <div className="displayResult">
                  <Row sm={12}>
                    <Col sm={{ span: 10, offset: 1 }}>
                      <div className="totalFee">
                        <h6>${result.total}</h6>
                        <span className="currency">{result.currency}</span>
                      </div>
                      {/* <p>{t("Home.Calculator.FormContent.Result.tax")}</p> */}
                      <Row className="calculatorTracking">
                        <Col sm={6}>
                          <p>
                            {t("Home.Calculator.FormContent.Result.tracking")}
                          </p>
                        </Col>
                        <Col sm={{ span: 2, offset: 2 }}>
                          <p className="tag">
                            {t("Home.Calculator.FormContent.Result.tag")}
                          </p>
                        </Col>
                      </Row>
                      <div className="breakdown">
                        {result.charge_detail.map((item) => {
                          return (
                            <Row key={item.title}>
                              <Col sm={8}>{item.title}</Col>
                              <Col sm={4}>${item.amount}</Col>
                            </Row>
                          );
                        })}
                      </div>
                      <button
                        className="outlinedBttn bttn"
                        onClick={handleRefresh}
                      >
                        <FiRefreshCw />
                        {t("Home.Calculator.FormContent.Result.button")}
                      </button>

                      <div className="disclaimer">
                        <p>
                          {t("Home.Calculator.FormContent.Result.disclaimer")}
                        </p>
                      </div>
                    </Col>
                  </Row>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default Calculator;
