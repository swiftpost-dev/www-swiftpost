import React, { useContext, useState } from "react";
import {
  Accordion,
  AccordionContext,
  Button,
  Col,
  Form,
  FormControl,
  InputGroup,
  Row,
  useAccordionToggle,
} from "react-bootstrap";
import { Trans, useTranslation } from "react-i18next";
import { AiOutlineLoading3Quarters } from "react-icons/ai";
import { BsArrowRight } from "react-icons/bs";
import { FiAlertTriangle, FiExternalLink } from "react-icons/fi";
import { IoCloseOutline } from "react-icons/io5";
import { MdKeyboardArrowDown } from "react-icons/md";
import request from "request";

interface ContextAwareToggleInterface {
  children?: React.ReactNode;
  eventKey: string;
  callback?: any;
}

function ContextAwareToggle(props: ContextAwareToggleInterface) {
  const { t } = useTranslation();
  const currentEventKey = useContext(AccordionContext);

  const decoratedOnClick = useAccordionToggle(
    props.eventKey,
    () => props.callback && props.callback(props.eventKey)
  );

  const isCurrentEventKey = currentEventKey === props.eventKey;

  return (
    <button type="button" onClick={decoratedOnClick}>
      {t("Tracking.Result.button")}
      <MdKeyboardArrowDown
        className={isCurrentEventKey ? "collapseArrow" : undefined}
      />
    </button>
  );
}

interface TrackingEventsInterface {
  description: string;
  date: string;
  code: "TR" | "DEL" | "OF" | "EX";
}

interface ResultInterface {
  courier: string;
  vanity_url: string;
  tracking: string;
  link: string;
  events: Array<TrackingEventsInterface>;
}

function Tracking() {
  const { t } = useTranslation();

  const [input, setInput] = useState("");
  const [error, setError] = useState("");
  const [result, setResult] = useState<undefined | ResultInterface>(undefined);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  React.useEffect(() => {
    if (window.location.href.split("?")?.[1]?.split("t=")?.[1]) {
      setInput(window.location.href.split("?")?.[1]?.split("t=")?.[1] ?? "");
      fetchTracking(window.location.href.split("?")?.[1]?.split("t=")?.[1]);
    }
  }, []);

  const trackingState = {
    TR: [
      "/assets/tracking/tracking-state-processing@2x.png",
      "processing",
      "/assets/tracking/tracking-timeline-processing-large@2x.png",
    ],
    DEL: [
      "/assets/tracking/tracking-state-delivered@2x.png",
      "delivered",
      "/assets/tracking/tracking-timeline-delivered-large@2x.png",
    ],
    OF: [
      "/assets/tracking/tracking-state-outfordelivery@2x.png",
      "out for delivery",
      "/assets/tracking/tracking-timeline-outfordelivery-large@2x.png",
    ],
    EX: [
      "assets/tracking/tracking-state-error@2x.png",
      "error",
      "/assets/tracking/tracking-timeline-error-large@2x.png",
    ],
  };

  const fetchTracking = async (tracking: string) => {
    await request(
      `https://www.swiftpost.com/track/${tracking.trim()}`,
      function (error, response, body) {
        const parsedBody = JSON.parse(body);
        if (response.statusCode === 500) {
          setIsLoading(false);
          return setError(parsedBody.error);
        }
        if (response.statusCode === 200) {
          setIsLoading(false);
          return setResult(parsedBody);
        }
      }
    );
  };

  const handleSubmit = async (e: any) => {
    e?.preventDefault();
    setIsLoading(true);

    if (!input) {
      setIsLoading(false);
      return setError(t("Tracking.InputForm.Error.emptyInput"));
    }

    await fetchTracking(input);
  };

  const handleClear = () => {
    setInput("");
    setIsLoading(false);
    setError("");
  };

  const handleClose = () => {
    setResult(undefined);
    setIsLoading(false);
  };

  return (
    // <div className="trackingOuter">
    //   <div className="layoutContainer">
    <div className={result ? "trackingResult tracking" : "tracking"}>
      <div className="layoutContainer">
        <div className="upperContainer">
          <h1>{t("Tracking.title")}</h1>

          <Form>
            <InputGroup className={error ? "error" : undefined}>
              <FormControl
                placeholder="e.g. SPHD6D4004008KOA13371"
                aria-label="e.g. SPHD6D4004008KOA13371"
                aria-describedby={t("Tracking.InputForm.airaDescription")}
                value={input}
                onChange={(e) => {
                  setInput(e.currentTarget.value);
                  setError("");
                }}
              />
              {error ? (
                <InputGroup.Append>
                  <Button onClick={handleClear} type="button">
                    <IoCloseOutline />
                  </Button>
                </InputGroup.Append>
              ) : (
                <InputGroup.Append>
                  {!isLoading ? (
                    <Button type="submit" onClick={handleSubmit}>
                      <BsArrowRight />
                    </Button>
                  ) : (
                    <Button className="loader">
                      <AiOutlineLoading3Quarters />
                    </Button>
                  )}
                </InputGroup.Append>
              )}
            </InputGroup>
            {error ? (
              <div className="trackingError">
                <FiAlertTriangle color="#BA0007" />
                {error}
              </div>
            ) : null}
          </Form>
        </div>
      </div>

      <div className={result ? "lowerContainer showResult" : "lowerContainer"}>
        <div className="layoutContainer">
          <div
            className={
              result
                ? "trackingResultDisplay showResult"
                : "trackingResultDisplay"
            }
          >
            {result ? (
              <div>
                <div className="topBar">
                  <p>{t("Tracking.Result.title")}</p>
                  <button onClick={handleClose}>
                    <IoCloseOutline />
                  </button>
                </div>
                <div className="resultContent">
                  <p className="courrier">{result.courier}</p>
                  <p className="trackingNum">{result.tracking}</p>
                  <a
                    href={result.link}
                    target="_blank"
                    className="trackingLink"
                    rel="noreferrer"
                  >
                    {`TRACK ON ${result.vanity_url}`} <FiExternalLink />
                  </a>
                  <div className="divider"></div>
                  <Row>
                    <Col sm={12} md={2}>
                      <div className="statusContainer">
                        <img
                          src={trackingState[result["events"][0]["code"]][0]}
                          alt={trackingState[result["events"][0]["code"]][1]}
                        />
                      </div>
                    </Col>
                    <Col sm={12} md={10}>
                      <p className="location">
                        {result["events"][0]["description"]}
                      </p>
                      <p className="time">{result["events"][0]["date"]}</p>
                    </Col>
                  </Row>
                  <Accordion>
                    <ContextAwareToggle eventKey="0">
                      View all events
                    </ContextAwareToggle>
                    <Accordion.Collapse eventKey="0">
                      <div>
                        <div className="timeLine">
                          {result.events.map((event, i) => {
                            return (
                              <div className="container" key={i} style={{}}>
                                <div className="dot">
                                  <img
                                    src={trackingState[event.code][2]}
                                    alt=""
                                  />
                                </div>
                                <div className="content">
                                  <p>{event.description}</p>
                                  <p className="time">{event.date}</p>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                        <Row className="contact">
                          <Col
                            className="iconContainer"
                            sm={12}
                            md={{ offset: 1, span: 2 }}
                          >
                            <img
                              src="/assets/tracking/tracking-cta-questions@2x.png"
                              alt={t("Tracking.Contact.imageAlt")}
                            />
                          </Col>
                          <Col sm={12} md={9} className="contactText">
                            <Trans i18nKey="Tracking.Contact.description">
                              Have questions about this package? Our customer
                              support team is here to help! Email us at
                              <a
                                className="link"
                                href="mailto:support@swiftpost.com"
                              >
                                support@swiftpost.com
                              </a>
                              , or give us a call at
                              <a className="link" href="tel:18886556258">
                                1-888-655-6258
                              </a>
                              . Our agents are available Monday to Friday
                              9am–9pm EST, and Saturday & Sunday 9am–2pm EST.
                            </Trans>
                          </Col>
                        </Row>
                      </div>
                    </Accordion.Collapse>
                  </Accordion>
                </div>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    </div>
    //   </div>
    // </div>
  );
}

export default Tracking;
