import React from "react";
import useIsInViewport from "use-is-in-viewport";

const SlideAnimate = (props: any) => {
  const [isInViewport, targetRef] = useIsInViewport();
  const [alreadyViewed, setAlreadyViewed] = React.useState<boolean>(false);
  React.useEffect(() => {
    if (isInViewport && !alreadyViewed) setAlreadyViewed(true);
  }, [isInViewport, alreadyViewed]);
  return (
    <div>
      {props.children.map((child: any, count: number) => (
        <div
          ref={targetRef}
          key={`SlideAnimation_${count}`}
          className={`animated_item ${alreadyViewed ? `animate` : ``}`}
        >
          {child}
        </div>
      ))}
    </div>
  );
};

export default SlideAnimate;
