import React from "react";
import { Trans, useTranslation } from "react-i18next";
import beverageCard1 from "../../assets/segment/segment-beverage1-card.json";
import beverageCard2 from "../../assets/segment/segment-beverage2-card.json";
import SegmentCTA from "../templates/SegmentCTA";
import SegmentCurvedBg from "../templates/SegmentCurvedBg";
import SegmentFourSections from "../templates/SegmentFourSections";
import SegmentTitle from "../templates/SegmentTitle";
import SegmentWhy from "../templates/SegmentWhy";

function SegmentBeverage() {
  const { t } = useTranslation();
  const segmentWhyData = [
    {
      num: (
        <Trans
          i18nKey="Beverage.WhySwiftpost.Stat.PostalCount.num"
          values={{ postalCount: "900K" }}
        />
      ),
      description: t("Beverage.WhySwiftpost.Stat.PostalCount.description"),
    },
    {
      num: (
        <Trans
          i18nKey="Beverage.WhySwiftpost.Stat.Center.num"
          values={{ centerNum: "700+" }}
        />
      ),
      description: t("Beverage.WhySwiftpost.Stat.Center.description"),
    },
    {
      num: (
        <Trans
          i18nKey="Beverage.WhySwiftpost.Stat.Reduction.num"
          values={{ reductionNum: "50%" }}
        />
      ),
      description: t("Beverage.WhySwiftpost.Stat.Reduction.description"),
    },
  ];

  const segmentWhyBottomText =
    "Alcohol producers trust Swiftpost to deliver their " +
    (
      <span className="yellow">
        'fragile and age-restricted beverages with care. '
      </span>
    );

  const segmentWhyImages = [
    {
      url: "/assets/segment/NB_HEADER_white-resized.png",
      alt: t("Beverage.WhySwiftpost.Partner.ImageAlt.nickelBrook"),
    },
    {
      url: "/assets/segment/Pillitteri-white-resized.png",
      alt: t("Beverage.WhySwiftpost.Partner.ImageAlt.pillitteri"),
    },
    {
      url: "/assets/segment/pondviewwhitelogo-resized.png",
      alt: t("Beverage.WhySwiftpost.Partner.ImageAlt.pondview"),
    },
    {
      url: "/assets/segment/SP-EDIT-lakeofbayswhite-resized.png",
      alt: t("Beverage.WhySwiftpost.Partner.ImageAlt.lakeOfBay"),
    },
    // {
    //   url: "/assets/segment/segment-beverage-customer-example1@2x.png",
    //   alt: t("Beverage.WhySwiftpost.Partner.ImageAlt.muskokabrew"),
    // },
    {
      url: "/assets/segment/SP-EDIT-Malivoire_LOGO_WHITE-resized.png",
      alt: t("Beverage.WhySwiftpost.Partner.ImageAlt.malivoire"),
    },
  ];

  return (
    <div className="segmentBeverage">
      <SegmentTitle
        img="/assets/segment/segment-beverage-landing-video-firstframe.jpg"
        video="/assets/segment/segment-beverage-landing-video.mp4"
        title={t("Beverage.Header.title")}
        description={t("Beverage.Header.description")}
        color="#FFF"
      />
      <SegmentCurvedBg
        img="/assets/segment/segment-beverage-bg-curve-bottom-cream70@2x.png"
        bgcolor="rgba(237, 234, 192, 0.7)"
        color="#004b9f"
      >
        <p className="description">
          <Trans i18nKey="Beverage.CurvedBg.description">
            Fast and reliable shipping
            <span className="underlinedText">at a fair price</span>
          </Trans>
        </p>
      </SegmentCurvedBg>
      <SegmentFourSections
        reduceImg="/assets/segment/segment-beverage1-main.jpg"
        reduceImgAlt={t("Beverage.FourSections.Reduce.imageAlt")}
        reduceCard={beverageCard1}
        reduceTitle={t("Beverage.FourSections.Reduce.title")}
        reduceContent={t("Beverage.FourSections.Reduce.description")}
        simpleImg="/assets/segment/contactLessDelivery.jpeg"
        simpleImgAlt={t("Beverage.FourSections.Simple.imageAlt")}
        simpleCard={beverageCard2}
        simpleTitle={t("Beverage.FourSections.Simple.title")}
        simpleContent={t("Beverage.FourSections.Simple.description")}
        saveContent={t("Beverage.FourSections.Save.description")}
        contactImg="/assets/segment/segment-beverage-cta-contactsales@2x.png"
        contactImgAlt={t("Beverage.FourSections.Contact.imageAlt")}
      />
      <SegmentWhy statsData={segmentWhyData} images={segmentWhyImages}>
        <p className="text">
          <Trans i18nKey="Beverage.WhySwiftpost.Partner.description">
            Alcohol producers trust Swiftpost to deliver their
            <span className="yellowText">
              fragile and age-restricted beverages with care.
            </span>
          </Trans>
        </p>
      </SegmentWhy>
      <SegmentCTA illustration="/assets/segment/segment-beverage-cta-illustration@2x.png" />
    </div>
  );
}

export default SegmentBeverage;
