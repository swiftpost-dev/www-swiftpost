/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Col, Row } from "react-bootstrap";
import { Trans, useTranslation } from "react-i18next";
import { HashLink } from "react-router-hash-link";

function CompanyCTA() {
  const { t } = useTranslation();
  return (
    <div className="companyCTA">
      <div className="layoutContainer">
        <Row>
          <Col lg={6}>
            <h2>{t("Company.CTA.title")}</h2>
            <div className="ctaText">
              <div className="contact">
                <p className="unitTitle">
                  {t("Company.CTA.TalkToSales.title")}
                </p>
                <HashLink to="contact#top" className="link">
                  {t("Company.CTA.TalkToSales.contactForm")}
                </HashLink>
                <a href="tel:18886556258" className="link">
                  {t("Globals.PhoneNumber")}
                </a>
              </div>

              <div className="divider"></div>

              <div className="support">
                <p className="unitTitle">
                  {t("Company.CTA.CustomerSupport.title")}
                </p>
                <p>
                  <Trans i18nKey="Company.CTA.CustomerSupport.description">
                    Can’t find an answer in our
                    <a
                      href="http://support.swiftpost.com/"
                      target="_blank"
                      rel="noreferrer"
                      className="link"
                    >
                      Help & Support resources
                    </a>
                    ? Our agents are here to help! You can contact customer
                    support in a few ways:
                  </Trans>
                </p>

                <ol>
                  <li>
                    <a
                      // eslint-disable-next-line no-script-url
                      href="javascript:void(0)"
                      className="link"
                      onClick={() => {
                        // unable to avoid using ignore here
                        // @ts-ignore
                        window.$zopim.livechat.window.show();
                      }}
                    >
                      {t("Company.CTA.CustomerSupport.List.liveChat")}
                    </a>
                  </li>
                  <li>
                    {t("Company.CTA.CustomerSupport.List.email")}
                    <a href="mailto:support@swiftpost.com" className="link">
                      {t("Globals.SupportEmail")}
                    </a>
                  </li>
                  <li>
                    {t("Company.CTA.CustomerSupport.List.call")}
                    <a href="tel:18886556258" className="link">
                      {t("Globals.PhoneNumber")}
                    </a>
                  </li>
                </ol>

                <p>{t("Company.CTA.CustomerSupport.Hours.title")}</p>
                <div className="hour">
                  <Row>
                    <Col sm={6}>
                      <p>
                        {t("Company.CTA.CustomerSupport.Hours.Date.weekdays")}
                      </p>
                    </Col>
                    <Col sm={{ span: 6 }}>
                      <p>
                        {t("Company.CTA.CustomerSupport.Hours.Time.weekdays")}
                      </p>
                    </Col>
                  </Row>

                  <Row>
                    <Col sm={6}>
                      <p>
                        {t("Company.CTA.CustomerSupport.Hours.Date.weekends")}
                      </p>
                    </Col>
                    <Col sm={{ span: 6 }}>
                      <p>
                        {t("Company.CTA.CustomerSupport.Hours.Time.weekends")}
                      </p>
                    </Col>
                  </Row>
                </div>
              </div>

              <div className="divider"></div>

              <div className="mediaInquiries">
                <p className="unitTitle">
                  {t("Company.CTA.MediaInquiries.title")}
                </p>
                <p>
                  <Trans i18nKey="Company.CTA.MediaInquiries.description">
                    Writing about Swiftpost? Please
                    {/* <a href="#" className="link">
                      download our media kit
                    </a>
                    . For additional media inquiries, please */}
                    email us at{" "}
                    <a href="mailto:press@swiftpost.com" className="link">
                      press@swiftpost.com
                    </a>
                    .
                  </Trans>
                </p>
              </div>
            </div>
          </Col>

          <Col lg={{ span: 5, offset: 1 }}>
            <div className="mapContainer">
              <div className="bgContainer">
                <img
                  src="/assets/company/company-contact-cta-bg-map.jpg"
                  alt=""
                />
              </div>
              <div className="floating">
                <div className="imgContainer">
                  <img
                    src="/assets/company/company-contact-cta-smarthubmap@2x.png"
                    alt=""
                  />
                </div>

                <div className="text">
                  <p>{t("Company.CTA.SmarthubLocator.title")}</p>
                  <HashLink to="/#smarthub-locator" className="yellowText">
                    {t("Company.CTA.SmarthubLocator.button")}
                  </HashLink>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}
export default CompanyCTA;
