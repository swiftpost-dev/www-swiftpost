import React from "react";
import { Col, Row } from "react-bootstrap";
import placeholderTwitter from "../../assets/news/twitter-placeholder-small 1.png";
import { Trans, useTranslation } from "react-i18next";
import {AiOutlineArrowRight} from "react-icons/ai"

interface NewsInterface {
  img: string;
  tags: Array<string>;
  title: string;
}

interface NewsMainInterface {
  articleList: Array<NewsInterface>;
}

function NewsMain(props: NewsMainInterface) {
  const { t } = useTranslation();
  
  return (
    <div className="newsMain">
      <div className="layoutContainer">
        <Row>
          <Col sm={12} lg={8}>
            <div className="newsArticles">
              <h2>{t("News.title")}</h2>

              <Row>
                {props.articleList.map((article) => {
                  return (
                    <Col sm={12} md={5} className="articleCard">
                      <div className="imgContainer">
                        <img src={article.img} alt="" />
                      </div>
                      <div className="tags">
                        {article.tags.map((tag, i) => {
                          return (
                            <a href="#" className="tag" key={i}>
                              {tag}
                            </a>
                          );
                        })}
                      </div>

                      <p>{article.title}</p>
                      <a href="#" className="readmore">
                      {t("News.Main.readMore")} <AiOutlineArrowRight />
                      </a>
                    </Col>
                  );
                })}
              </Row>

              <button className="bttn outlinedBttn">{t("News.Main.loadMore")}</button>
            </div>
          </Col>

          <Col lg={4}>
            <div className="newsMedia">
              <Row>
                <Col className="text" sm={12} md={5} lg={12}>
                  <h3>{t("News.Main.MediaInquiries.title")}</h3>

                  <p>
                    <Trans i18nKey="News.Main.MediaInquiries.description">
                      Writing about Swiftpost? Please
                      {/* <a href="#" className="link">
                        download our media kit
                      </a>
                      . For additional media inquiries, please */}
                      email us at
                      <a href="mailto:press@swiftpost.com" className="link">
                        press@swiftpost.com
                      </a>
                      .
                    </Trans>
                  </p>
                </Col>

                <Col className="twitterTimeline" md={7} lg={12}>
                  {/* place holder image for twitter timeline */}
                  <img src="/assets/news/twitter-placeholder-small1.png" alt="" />
                  {/* the timeline library */}
                  {/* <Timeline
                  dataSource={{
                    sourceType: 'profile',
                    screenName: 'SwiftPost'
                  }}
                  options={{
                    height: "400px",
                  }}
                />  */}
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}
export default NewsMain;
