import React from "react";
import { HashLink } from "react-router-hash-link";

interface OnTopToSideInterface {
  img: string;
  titleText: string;
  contentText?: string;
  buttonText?: string;
}

function OnTopToSide(props: OnTopToSideInterface) {
  return (
    <div className="onTopToSide">
      <div className="text">
        <h2>{props.titleText}</h2>
        {props.buttonText ? (
          <HashLink to="contact#top" className="link">
            <button className="primaryBttn bttn">{props.buttonText}</button>
          </HashLink>
        ) : null}
      </div>
      <div className="imgContainer">
        <img src={props.img} alt="" />
      </div>
    </div>
  );
}

export default OnTopToSide;
