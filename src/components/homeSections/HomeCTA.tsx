import React from "react";
import { useTranslation } from "react-i18next";
import Lottie from "react-lottie";
import { HashLink } from "react-router-hash-link";

import ctaBg from "../../assets/main/home-bottomcta-bg.json";

function HomeCTA() {
  const { t } = useTranslation();
  return (
    <div className="homeCTA">
      <div className="homeCTABg">
        <Lottie
          options={{
            loop: true,
            autoplay: true,
            animationData: ctaBg,
            rendererSettings: {
              preserveAspectRatio: "xMidYMid slice",
            },
          }}
          height={"100%"}
          width={"100%"}
        />
      </div>
      <div className="layoutContainer">
        <h3>{t("Home.CTA.title")}</h3>
        <div className="bttnGroup">
          {/* <a
            href="https://app.swiftpost.com/signup"
            target="_blank"
            rel="noreferrer"
          >
            <button className="primaryBttn bttn">
              {t("Home.CTA.button.getStarted")}
            </button>
          </a> */}
          <HashLink to="contact#top">
            <button className="primaryBttn bttn">
              {t("Home.CTA.button.talkToSales")}
            </button>
          </HashLink>
        </div>
      </div>
    </div>
  );
}
export default HomeCTA;
