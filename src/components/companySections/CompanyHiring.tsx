import React from "react";
import { useTranslation } from "react-i18next";
import Lottie from "react-lottie";
import hiringBg from "../../assets/company/company-hiring-bg.json";

function CompanyHiring() {
  const { t } = useTranslation();
  return (
    <div className="companyHiring">
      <div className="lottieBg">
        <Lottie
          options={{
            loop: true,
            autoplay: true,
            animationData: hiringBg,
            rendererSettings: {
              preserveAspectRatio: "xMidYMin slice",
            },
          }}
          width={"100%"}
        />
      </div>
      <div className="layoutContainer">
        <div className="hiringTop">
          <img
            src="/assets/company/company-collage-small1.png"
            alt={t("Company.Hiring.ImageAlt.happyFace")}
          />
        </div>

        <div className="hiringML">
          <img
            src="/assets/company/company-collage-full.png"
            alt={t("Company.Hiring.ImageAlt.happyFace")}
          />
        </div>
        <div className="hiringCTA">
          <div className="iconContainer">
            <img
              src="/assets/company/company-hiring-cta@2x.png"
              alt={t("Company.Hiring.ImageAlt.logo")}
            />
          </div>
          <p>{t("Company.Hiring.description")}</p>
          <a href="mailto: careers@swiftpost.com" className="link">
            {t("Company.Hiring.button")}
          </a>
        </div>
      </div>
      {/* </div> */}
    </div>
  );
}
export default CompanyHiring;
