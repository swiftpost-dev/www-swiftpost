import React, {useState, useRef} from "react";
import useIsInViewport from "use-is-in-viewport";

interface HeaderSideImgInterface {
  img: string;
  video?: string;
  imgml?: string;
  videoml?: string;
  titleText: any; // I passed a h2 tag to this child component but for some reasons i couldn't use the htmlelement
  secondaryText?: string;
  buttonText?: string;
  children?: React.ReactNode;
}

function HeaderSideImg(props: HeaderSideImgInterface) {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth)
  const [isInViewport, targetRef] = useIsInViewport();

  React.useEffect(() => {
    const vid = document.querySelector("video")
    if (isInViewport){
       vid?.play()
       return
      }
      vid?.pause()
  }, [isInViewport]);

  React.useEffect(() => {
    function handleResize() {
      // Set window width/height to state
      setWindowWidth(window.innerWidth);
    }
    // Add event listener
    window.addEventListener("resize", handleResize);
    // Call handler right away so state gets updated with initial window size
    handleResize();
    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return (
    <div className="headerSideImgBgWrapper">
      <div className="layoutContainer">
        <div className="headerSideImg">
          
          <div className="sImgContainer">
              <img src={props.img} alt="" />
          </div>

          <div className="text">
            {props.titleText}
            {props.secondaryText ? <p>{props.secondaryText}</p> : null}
            {props.buttonText ? (
              <a
                href="https://app.swiftpost.com/signup"
                target="_blank"
                rel="noreferrer"
              >
                <button className="bttn primaryBttn">{props.buttonText}</button>
              </a>
            ) : null}
          </div>

          {props.videoml && windowWidth > 750 ? (
            <div className="floatingContainer right">
              <div className="mlImgContainer onBottom">
                <video
                  width="100%"
                  height="100%"
                  autoPlay
                  loop
                  poster={props.imgml}
                  muted
                  ref={targetRef}
                >
                  <source src={props.videoml} />
                </video>
              </div>
              <div className="onTop">{props.children}</div>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
}

export default HeaderSideImg;
