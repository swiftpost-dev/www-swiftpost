import React from "react";

import CompanyHeader from "../companySections/CompanyHeader";
import CompanyHiring from "../companySections/CompanyHiring";
import CompanyCommunity from "../companySections/CompanyCommunity";
import CompanyCarousels from "../companySections/CompanyCarousels";
import CompanyCTA from "../companySections/CompanyCTA";

function Company() {
  return (
    <div className="company">
      <CompanyHeader />
      <CompanyHiring />
      <CompanyCommunity />
      <CompanyCarousels />
      <CompanyCTA />
    </div>
  );
}

export default Company;
