export interface Smarthub { 
    id: string;
    address1: string;
    address2?: string;
    city: string;
    lat: number;
    lng: number;
    name: string;
    phone?: string;
    postal: string;
    state: string;
    picture_1?: string;
    picture_2?: string;
};

export interface Partner {
    img: string;
    alt: string;
};

export interface HowContent {
    img: string;
    title: string;
    description: string | any;
    link?: any;
};

export enum NavColorState {
    "PRIMARY",
    "LIGHT"
}

export interface Geocoords {
    coords: LatLong
}

interface LatLong {
    latitude: number,
    longitude: number
}