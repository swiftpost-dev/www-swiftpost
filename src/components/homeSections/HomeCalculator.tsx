import React from "react";
import { Col, Row } from "react-bootstrap";
import Lottie from "react-lottie";
import { useTranslation, Trans } from "react-i18next";
import { HashLink } from "react-router-hash-link";
import useIsInViewport from "use-is-in-viewport";

import LottieContainer from "../templates/LottieContainer";
import Calculator from "../templates/Calculator";

import calculatorIcon from "../../assets/main/home-ratecalculator-icon.json";

function HomeCalCulator() {
  const { t } = useTranslation();

  return (
    <div className="homeCalculator">
      <Row className="layoutContainer">
        <Col className="texts" sm={12} md={5} lg={5}>
          <div className="imgContainer">
            <LottieContainer icon={calculatorIcon} width={140} height={170} />
          </div>
          <h6>{t("Home.Calculator.title")}</h6>
          <p>
            <Trans i18nKey="Home.Calculator.description">
              Get an honest estimate, with no hidden fees.
              <HashLink to="contact#top" className="link">
                Contact us
              </HashLink>
              for a custom quote.
            </Trans>
          </p>
        </Col>

        <Col md={7} lg={{span: 5, offset: 1}}>
          <Calculator />
        </Col>
      </Row>
    </div>
  );
}
export default HomeCalCulator;
