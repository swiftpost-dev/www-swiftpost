import React from "react";
import { Row, Col } from "react-bootstrap";
import { useTranslation } from "react-i18next";

function NewsSort() {
  const { t } = useTranslation();

  const categories = [
    t("News.Sort.all"),
    t("News.Sort.ecommerce"),
    t("News.Sort.beverage"),
    t("News.Sort.healthcare"),
    t("News.Sort.companyNews"),
    t("News.Sort.shippingTips"),
  ];
  return (
    <div className="newsSort">
      <div className="layoutContainer">
        <p className="title">sort articles</p>

        <Row className="categories">
          {categories.map((category, i) => {
            return (
              <Col sm={6} className="category">
                <a href="#" key={i}>
                  {category}
                </a>

                <span className="divider"></span>
              </Col>
            );
          })}
        </Row>
      </div>
    </div>
  );
}
export default NewsSort;
