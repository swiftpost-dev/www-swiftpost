import React from "react";
import { useTranslation } from "react-i18next";
import { HashLink } from "react-router-hash-link";

interface SegmentCurvedBgInterface {
  img: string;
  // text: string;
  // text1: string;
  // text2: string;
  // text3: string;
  color: string;
  bgcolor: string;
  children: React.ReactNode;
}

function SegmentCurvedBg(props: SegmentCurvedBgInterface) {
  const { t } = useTranslation();

  return (
    <div className="segmentCurvedBg">
      <div
        className="textContainer"
        style={{ background: props.bgcolor, color: props.color }}
      >
        <div className="layoutContainer">
          {props.children}
          <HashLink to="contact#top">
            <button className="primaryBttn bttn">
              {t("SegmentsCommon.CurvedBg.button")}
            </button>
          </HashLink>
        </div>
      </div>
      {/* <div className="curveContainer">
        <img src={props.img} alt="" />
      </div> */}
    </div>
  );
}

export default SegmentCurvedBg;
