import React from "react";
import { Trans, useTranslation } from "react-i18next";
import ecommerceCard1 from "../../assets/segment/segment-ecommerce1-card.json";
import ecommerceCard2 from "../../assets/segment/segment-ecommerce2-card.json";
import SegmentCTA from "../templates/SegmentCTA";
import SegmentCurvedBg from "../templates/SegmentCurvedBg";
import SegmentFourSections from "../templates/SegmentFourSections";
import SegmentTitle from "../templates/SegmentTitle";
import SegmentWhy from "../templates/SegmentWhy";

function SegmentEcommerce() {
  const { t } = useTranslation();
  const segmentWhyData = [
    {
      num: (
        <Trans
          i18nKey="Ecommerce.WhySwiftpost.Stat.ResponseTime.num"
          values={{ responseTime: "32s" }}
        />
      ),
      description: t("Ecommerce.WhySwiftpost.Stat.ResponseTime.description"),
    },
    {
      num: (
        <Trans
          i18nKey="Ecommerce.WhySwiftpost.Stat.PostalCount.num"
          values={{ postalCount: "900K" }}
        />
      ),
      description: t("Ecommerce.WhySwiftpost.Stat.PostalCount.description"),
    },
    {
      num: (
        <Trans
          i18nKey="Ecommerce.WhySwiftpost.Stat.MontlyFee.num"
          values={{ monthlyFee: "0$" }}
        />
      ),
      description: t("Ecommerce.WhySwiftpost.Stat.MontlyFee.description"),
    },
  ];

  const segmentWhyImages = [
    {
      url: "/assets/segment/SP-EDIT-kt8-main-logo-white-resized.png",
      alt: t("Ecommerce.WhySwiftpost.Partner.ImageAlt.kt8"),
    },
    {
      url: "/assets/segment/TRUE-NORTH-LOGO-01-white-resized.png",
      alt: t("Ecommerce.WhySwiftpost.Partner.ImageAlt.trueNorth"),
    },
    // {
    //   url: "/assets/segment/beaus-brewing-company-white-resized.png",
    //   alt: t("Ecommerce.WhySwiftpost.Partner.ImageAlt.beaus"),
    // },
    {
      url: "/assets/segment/SP-EDIT-cheerfullymadelgo-White-resized.png",
      alt: t("Ecommerce.WhySwiftpost.Partner.ImageAlt.cheerfullyMade"),
    },
    // {
    //   url: "/assets/segment/steepedteasipologywhite-resized.png",
    //   alt: t("Ecommerce.WhySwiftpost.Partner.ImageAlt.sipology"),
    // },
    // {
    //   url: "/assets/segment/SP-EDIT-stratus-logo-white-resized.png",
    //   alt: t("Ecommerce.WhySwiftpost.Partner.ImageAlt.stratus"),
    // },
  ];

  return (
    <div className="segmentEcommerce">
      <SegmentTitle
        img="/assets/segment/segment-ecommerce-landing-video-firstframe.jpg"
        video="/assets/segment/segment-ecommerce-landing-video.mp4"
        title={t("Ecommerce.Header.title")}
        description={t("Ecommerce.Header.description")}
        color="#004B9F"
      />
      <SegmentCurvedBg
        img="/assets/segment/segment-ecommerce-bg-curve-bottom-deepblue@2x.png"
        bgcolor="#004B9F"
        color="#FFF"
      >
        <p className="description">
          <Trans i18nKey="Ecommerce.CurvedBg.description">
            Scale your ecommerce business with
            <span className="underlinedText">
              a modern, reliable transportation partner.
            </span>
          </Trans>
        </p>
      </SegmentCurvedBg>
      <SegmentFourSections
        reduceImg="/assets/segment/segment-ecommerce1-main.jpg"
        reduceImgAlt={t("Ecommerce.FourSections.Reduce.imageAlt")}
        reduceCard={ecommerceCard1}
        reduceTitle={t("Ecommerce.FourSections.Reduce.title")}
        reduceContent={t("Ecommerce.FourSections.Reduce.description")}
        simpleImg="/assets/segment/segment-ecommerce2-main.jpg"
        simpleImgAlt={t("Ecommerce.FourSections.Simple.imageAlt")}
        simpleCard={ecommerceCard2}
        simpleTitle={t("Ecommerce.FourSections.Simple.title")}
        simpleContent={t("Ecommerce.FourSections.Simple.description")}
        saveContent={t("Ecommerce.FourSections.Save.description")}
        contactImg="/assets/segment/segment-ecommerce-cta-contactsales@2x.png"
        contactImgAlt={t("Ecommerce.FourSections.Contact.imageAlt")}
      />
      <SegmentWhy statsData={segmentWhyData} images={segmentWhyImages}>
        <p className="text">
          <Trans i18nKey="Ecommerce.WhySwiftpost.Partner.description">
            Ecommerce sellers of all sizes trust Swiftpost to
            <span className="yellowText">
              reliably deliver their brand to the world.
            </span>
          </Trans>
        </p>
      </SegmentWhy>
      <SegmentCTA illustration="/assets/segment/segment-ecommerce-cta-illustration@2x.png" />
    </div>
  );
}

export default SegmentEcommerce;
