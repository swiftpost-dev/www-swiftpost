import React from "react";
import { Row, Col } from "react-bootstrap";
import { useTranslation, Trans } from "react-i18next";

function ReferralsHow() {
  const { t } = useTranslation();

  const howText = [
    {
      num: "1.",
      title: t("Referral.How.Description.GetYourLink.title"),
      content: (
        <p>
          <Trans i18nKey="Referral.How.Description.GetYourLink.description">
            <a
              href="https://app.swiftpost.com/signup"
              target="_blank"
              rel="noreferrer"
              className="link"
            >
              Sign up to Swiftpost
            </a>
            , log in, and generate your referral link from your “Account”
            dashboard. This referral link lorem ipsum is also found in your
            welcome email.
          </Trans>
        </p>
      ),
    },
    {
      num: "2.",
      title: t("Referral.How.Description.InviteFriends.title"),
      content: (
        <p>
          {t("Referral.How.Description.InviteFriends.description")}
        </p>
      ),
    },
    {
      num: "3.",
      title: t("Referral.How.Description.EarnRewards.title"),
      content: (
        <p>
          {t("Referral.How.Description.EarnRewards.description")}
        </p>
      ),
    },
  ];

  return (
    <div className="referralsHow">
      <div className="layoutContainer">
        <p className="smallTitle">{t("Referral.How.title")}</p>
        <Row>
          {howText.map((text, i) => {
            return (
              <Col sm={12} md={4} className="contentCol">
                <h2>{text.num}</h2>
                <h5>{text.title}</h5>
                {text.content}
              </Col>
            );
          })}
        </Row>
      </div>
    </div>
  );
}
export default ReferralsHow;
