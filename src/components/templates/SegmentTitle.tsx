import React, {useState} from "react";
import { useTranslation } from "react-i18next";
import { HashLink } from "react-router-hash-link";
import useIsInViewport from "use-is-in-viewport";

interface SegmentTitleInterface {
  video: string;
  img: string;
  description: string;
  title: string;
  color: string;
}

function SegmentTitle(props: SegmentTitleInterface) {
  const { t } = useTranslation();
  const [windowWidth, setWindowWidth] = useState(window.innerWidth)
  const [isInViewport, targetRef] = useIsInViewport();

  React.useEffect(() => {
    const vid = document.querySelector("video")
    if (isInViewport){
       vid?.play()
       return
      }
      vid?.pause()
  }, [isInViewport]);

  React.useEffect(() => {
    function handleResize() {
      // Set window width/height to state
      setWindowWidth(window.innerWidth);
    }
    // Add event listener
    window.addEventListener("resize", handleResize);
    // Call handler right away so state gets updated with initial window size
    handleResize();
    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  return (
    <div className="segmentTitle">
      {
        windowWidth < 750 ? <div className="imgContainer small">
        <img src={props.img} alt="" />
      </div> : <div className="imgContainer ml">
        <video width="100%" height="100%" autoPlay loop poster={props.img} muted ref={targetRef}>
          <source src={props.video} />
        </video>
      </div>
      }
      
      <div className="layoutContainer">
        <div className="textContainer">
          <p>{props.title}</p>
          <p className="description">{props.description}</p>
          <HashLink to="contact#top">
            <button className="primaryBttn bttn">
              {t("SegmentsCommon.SegmentTitle.button")}
            </button>
          </HashLink>
        </div>
      </div>
    </div>
  );
}

export default SegmentTitle;
