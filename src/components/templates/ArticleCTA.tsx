import React from "react";

import emailLogo from "../../assets/articleOne/news-share-email@2x.png";
import fbLogo from "../../assets/articleOne/news-share-facebook@2x.png";
import linkedInLogo from "../../assets/articleOne/news-share-linkedin@2x.png";
import twitterLogo from "../../assets/articleOne/news-share-twitter@2x.png";

import { ImTwitter, ImLinkedin } from "react-icons/im";
import { FaFacebook } from "react-icons/fa";
import {MdEmail} from "react-icons/md"

function ArticleCTA() {
  return (
    <div className="articleCTA">
      <div className="ctaTop">
        <p>Share this article</p>
        <div className="share">
          <a href="https://twitter.com/share?url=<URL>&text=<TEXT>via=<USERNAME>">
            <ImTwitter />
          </a>
          <a href="https://www.facebook.com/sharer/sharer.php?u=<URL>">
            <FaFacebook />
          </a>
          <a href="https://www.linkedin.com/shareArticle?url=<URL>&title=<TITLE>&summary=<SUMMARY>&source=<SOURCE_URL>">
            <ImLinkedin />
          </a>
          <a href="mailto:?subject=<SUBJECT>&body=<BODY>">
            <MdEmail />
          </a>
        </div>
      </div>
    </div>
  );
}

export default ArticleCTA;
