import React from "react";

import { Row, Col } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import {AiOutlineArrowRight} from "react-icons/ai"

interface NewsInterface {
  img: string;
  tags: Array<string>;
  title: string;
}

function NewsHeadline(props: NewsInterface) {
  const { t } = useTranslation();

  return (
    <div className="newsHeadline">
      <div className="layoutContainer">
        <Row>
          <Col sm={12} lg={5} className="imgContainer">
            <img src={props.img} alt="" />
          </Col>

          <Col>
            <div className="tags">
              {props.tags.map((tag, i) => {
                return (
                  <a href="#" className="tag" key={i}>
                    {tag}
                  </a>
                );
              })}
            </div>

            <h2>{props.title}</h2>

            <a href="#" className="yellowText readmore">
              {t("News.HeadLine.button")} <AiOutlineArrowRight />
            </a>
          </Col>
        </Row>
      </div>
    </div>
  );
}
export default NewsHeadline;
