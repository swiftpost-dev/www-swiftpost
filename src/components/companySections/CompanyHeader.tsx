import React from "react";
import { useTranslation } from "react-i18next";
import Lottie from "react-lottie";
import landingBg from "../../assets/company/company-landing-bg.json";

function CompanyHeader() {
  const { t } = useTranslation();
  return (
    <div className="companyLanding">
      <div className="lottieBg">
        <Lottie
          options={{
            loop: true,
            autoplay: true,
            animationData: landingBg,
            rendererSettings: {
              preserveAspectRatio: "xMidYMin slice",
            },
          }}
          width={"100%"}
        />
      </div>
      <div className="layoutContainer">
        <p className="smallTitle">{t("Company.Header.title")}</p>
        <p className="largerText">{t("Company.Header.description")}</p>
        <div className="imgContainer">
          <img
            src="/assets/company/company-swiftpost-truck.png"
            alt={t("Company.Header.imageAlt")}
          />
        </div>
        <div className="columnsContainer two">
          <div className="singleColumn ">
            <p className="unitTitle">
              {t("Company.Header.Value.Difference.title")}
            </p>
            <p>{t("Company.Header.Value.Difference.description")}</p>
          </div>

          <div className="singleColumn">
            <p className="unitTitle">
              {t("Company.Header.Value.Positive.title")}
            </p>
            <p>{t("Company.Header.Value.Positive.description")}</p>
          </div>
        </div>
      </div>
    </div>
  );
}
export default CompanyHeader;
