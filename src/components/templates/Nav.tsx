import React from "react";
import { Col, Row } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { AiOutlineMenu } from "react-icons/ai";
import { IoIosArrowForward } from "react-icons/io";
import { IoCloseOutline } from "react-icons/io5";
import { MdKeyboardArrowDown } from "react-icons/md";
import { matchPath, useLocation } from "react-router-dom";
import { HashLink } from "react-router-hash-link";
import { NavColorState } from "../../types";

function Nav() {
  const { t } = useTranslation();
  const location = useLocation();
  const [navColor, setNavColor] = React.useState<NavColorState>(
    NavColorState.LIGHT
  );

  const scrollWithOffset = (el: any) => {
    const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
    const yOffset = -70;
    window.scrollTo({ top: yCoordinate + yOffset, behavior: "smooth" });
  };

  const scrollListener = () => {
    if (window.scrollY < 150 && navColor === NavColorState.LIGHT) {
      setNavColor(NavColorState.PRIMARY);
    } else if (window.scrollY > 150) {
      setNavColor(NavColorState.LIGHT);
    }
  };
  React.useEffect(() => {
    const isHomepage = matchPath(location.pathname, {
      path: "/",
      exact: true,
      strict: true,
    });
    if (isHomepage) {
      if (window.scrollY < 150) {
        setNavColor(NavColorState.PRIMARY);
      }
      window.addEventListener("scroll", scrollListener);
    } else {
      setNavColor(NavColorState.LIGHT);
    }
    return () => {
      window.removeEventListener("scroll", scrollListener);
    };
  }, [location, navColor]);

  const [menuClose, setMenuClose] = React.useState(true);

  const dropdownList: Array<"Ecommerce" | "Healthcare" | "Beverage"> = [
    t("Navigation.Ecommerce"),
    t("Navigation.Healthcare"),
    t("Navigation.Beverage"),
  ];

  const closeMenu = () => {
    setMenuClose(true);
  };

  const openMenu = () => {
    setMenuClose(false);
  };

  return (
    <div
      className={`header ${
        navColor === NavColorState.PRIMARY ? `primary` : ``
      }`}
    >
      <header>
        <div className="headerLogo">
          <HashLink to="/#top">
            <img
              src={
                navColor === NavColorState.PRIMARY
                  ? "/assets/header/logo-swiftpost-white@2x.png"
                  : "/assets/header/logo-swiftpost@2x.png"
              }
              alt=""
            />
          </HashLink>
        </div>

        <Row className="align-items-center">
          <Col md={"auto"}>
            <div className="mobileMenuIcon" onClick={openMenu}>
              <AiOutlineMenu
                className={NavColorState.PRIMARY ? "primaryNav" : "lightNav"}
              />
            </div>

            {/* {menuClose ? null : ( */}
            <div className={menuClose ? "mobileNav hide" : "mobileNav show"}>
              <div className="closeIcon" onClick={closeMenu}>
                <IoCloseOutline />
              </div>
              <ul>
                <li>
                  <HashLink
                    smooth
                    to="/#how-it-works"
                    scroll={(el) => scrollWithOffset(el)}
                    onClick={closeMenu}
                  >
                    {t("Navigation.HowItWorks")}
                  </HashLink>
                </li>
                <li>
                  <input type="checkbox" id="solutions" />
                  <label htmlFor="solutions" className="dropdownLabel">
                    {t("Navigation.Solutions")}
                    <MdKeyboardArrowDown />
                  </label>
                  <div className="dropdown">
                    <HashLink to="ecommerce#top" onClick={closeMenu}>
                      {t("Navigation.Ecommerce")}
                    </HashLink>
                    <HashLink to="healthcare#top" onClick={closeMenu}>
                      {t("Navigation.Healthcare")}
                    </HashLink>
                    <HashLink to="beverage#top" onClick={closeMenu}>
                      {t("Navigation.Beverage")}
                    </HashLink>
                  </div>
                </li>
                <li>
                  <HashLink to="tracking#top" onClick={closeMenu}>
                    {t("Navigation.Tracking")}
                  </HashLink>
                </li>
                <li>
                  <input type="checkbox" id="support" />
                  <label htmlFor="support" className="dropdownLabel">
                    {t("Navigation.Support")}
                    <MdKeyboardArrowDown />
                  </label>
                  <div className="dropdown">
                    <a
                      href="javascript:void(0)"
                      onClick={() => {
                        closeMenu();
                        // unable to avoid using ignore here
                        // @ts-ignore
                        window.$zopim.livechat.window.show();
                      }}
                    >
                      {t("Navigation.LiveChat")}
                    </a>
                    <a
                      href="http://support.swiftpost.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      {t("Navigation.HelpCenter")}
                    </a>
                  </div>
                </li>
                <li>
                  <input type="checkbox" id="contact" />
                  <label htmlFor="contact" className="dropdownLabel">
                    {t("Navigation.ContactSales")}
                    <MdKeyboardArrowDown />
                  </label>
                  <div className="dropdown">
                    <HashLink to="contact#top" onClick={closeMenu}>
                      {t("Navigation.EmailUs")}
                    </HashLink>
                    <a href="tel:18886556258">{t("Globals.PhoneNumber")}</a>
                  </div>
                </li>
              </ul>

              <div className="headerCTA">
                <a
                  href="https://app.swiftpost.com/login"
                  target="_blank"
                  rel="noreferrer"
                >
                  <button className="bttn outlinedBttn">
                    {t("Navigation.LogIn")}
                  </button>
                </a>
                <a
                  href="https://app.swiftpost.com/signup"
                  target="_blank"
                  rel="noreferrer"
                >
                  <button className="bttn primaryBttn">
                    {t("Navigation.SignUp")}
                  </button>
                </a>
              </div>
            </div>
            {/* )} */}
          </Col>

          {/* <Col> */}
          <Col className="menuRight">
            <input type="checkbox" id="sales" />
            <label htmlFor="sales" className="dropdownLabel">
              {t("Navigation.Sales")}
              <MdKeyboardArrowDown />
            </label>
            <div className="dropdown">
              <ul>
                <li>
                  <HashLink to="contact#top">
                    {t("Navigation.EmailUs")}
                    <div className="yellowArrow">
                      <IoIosArrowForward />
                    </div>
                  </HashLink>
                </li>
                <li>
                  <a href="tel:18886556258">
                    {t("Globals.PhoneNumber")}
                    <div className="yellowArrow">
                      <IoIosArrowForward />
                    </div>
                  </a>
                </li>
              </ul>
            </div>
            <div className="verticalDivider"></div>
            <div className="headerCTA">
              <a
                href="https://app.swiftpost.com/"
                target="_blank"
                rel="noreferrer"
              >
                <button>{t("Navigation.LogIn")}</button>
              </a>
              <a
                href="https://app.swiftpost.com/signup"
                target="_blank"
                rel="noreferrer"
              >
                <button className="primaryBttn bttn">
                  {t("Navigation.SignUp")}
                </button>
              </a>
            </div>
          </Col>
          {/* </Col> */}
        </Row>

        <nav className="mainMenu">
          <ul>
            <li>
              <HashLink
                smooth
                to="/#how-it-works"
                scroll={(el) => scrollWithOffset(el)}
              >
                {t("Navigation.HowItWorks")}
              </HashLink>
            </li>
            <li>
              <HashLink to="tracking#top">{t("Navigation.Tracking")}</HashLink>
            </li>
            <li className="dropdownBttn">
              <input type="checkbox" id="solutionsML" />
              <label htmlFor="solutionsML" className="dropdownLabel">
                {t("Navigation.Solutions")}
                <MdKeyboardArrowDown />
              </label>
              <div className="dropdown">
                <ul>
                  {dropdownList.map((item, i) => {
                    return (
                      <li key={i}>
                        <HashLink to={`${item.toLowerCase()}#top`}>
                          {item}
                          <div className="yellowArrow">
                            <IoIosArrowForward />
                          </div>
                        </HashLink>
                      </li>
                    );
                  })}
                </ul>
              </div>
            </li>
            <li className="dropdownBttn">
              <input type="checkbox" id="supportML" />
              <label htmlFor="supportML" className="dropdownLabel">
                {t("Navigation.Support")}
                <MdKeyboardArrowDown />
              </label>
              <div className="dropdown">
                <ul>
                  <li>
                    <a
                      href="javascript:void(0)"
                      onClick={() => {
                        // unable to avoid using ignore here
                        // @ts-ignore
                        window.$zopim.livechat.window.show();
                      }}
                    >
                      {t("Navigation.LiveChat")}
                      <div className="yellowArrow">
                        <IoIosArrowForward />
                      </div>
                    </a>
                  </li>
                  <li>
                    <a
                      href="http://support.swiftpost.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      {t("Navigation.HelpCenter")}
                      <div className="yellowArrow">
                        <IoIosArrowForward />
                      </div>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
        </nav>
      </header>
    </div>
  );
}

export default Nav;
