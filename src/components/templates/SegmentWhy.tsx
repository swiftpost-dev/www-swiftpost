import React from "react";
import { useTranslation, Trans } from "react-i18next";

import Stats from "../templates/Stats";

interface DataInterface {
  num: any;
  description: string;
}

interface ImgInterface {
  url: string;
  alt: string;
}

interface SegmentWhyInterface {
  statsData: Array<DataInterface>;
  images: Array<ImgInterface>;
  children: React.ReactNode;
}

function SegmentWhy(props: SegmentWhyInterface) {
  const { t } = useTranslation();
  return (
    <div className="segmentWhyOuter">
      <div className="layoutContainer">
        <div className="segmentWhy">
          <p className="title">Why choose Swiftpost?</p>

          <Stats statsData={props.statsData} />

          <div className="divider horizon"></div>

          <div className="bottom">
            {props.children}

            <div className="companiesContainer">
              {props.images.map((img, i) => {
                return (
                  <div key={i} className="company">
                    <img src={img.url} alt={img.alt} />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SegmentWhy;
