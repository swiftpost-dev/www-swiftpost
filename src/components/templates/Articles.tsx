import React from "react";
import { Row, Col } from "react-bootstrap";

import ArticleCTA from "./ArticleCTA";


interface AuthorInfoInterface {
  avatar: string;
  name: string;
  date: string;
}
interface HangingQuotationInterface {
  avatar: string;
  content: string;
  author: string;
  authorTitle: string;
  association: string;
}

interface ImgInbetweenInterface {
  img: string;
  caption: any;
}

interface SubsectionInterface {
  title: string;
  content: string;
}

interface ArticlesProps {
  cover?: string;
  title: string;
  tags: Array<string>;
  author?: AuthorInfoInterface;
  // content: Array<any>;
  contentBttm?: Array<any>;
  hangingQuotation?: HangingQuotationInterface;
  imgInbetween?: ImgInbetweenInterface;
  subsection?: Array<SubsectionInterface>;
  signup: boolean;
  children: React.ReactNode;
}

function Articles(props: ArticlesProps) {
  return (
    <div className="articles">
      <div className="articlesTitle">
        <div className="articlesBg">
          <div className="titleText">
            <div className="layoutContainer">
              <div className="tags">
                {props.tags.map((tag, i) => {
                  return (
                    <a href="#" className="tag" key={i}>
                      {tag}
                    </a>
                  );
                })}
              </div>

              <h1>{props.title}</h1>
            </div>
          </div>

          <div className="curvedBg">
            <img src="/assets/articleOne/news-bg-curve-bottom-cyan25@2x.png" alt="" />
          </div>
        </div>
      </div>

      <div className="articleContent">
        <div className="layoutContainer">
          <div className="articleMain">
            <Row className="mainTop">
              <Col className="coverContainer" sm={12} lg={8}>
                <img src={props.cover} alt="" />
              </Col>

              {props.author ? (
                <Col lg={3}>
                  <Row className="infoContainer">
                    <Col className="author" sm={12} md={7} lg={12}>
                      <div className="avatar">
                        <img src={props.author.avatar} alt="" />
                      </div>
                      <p>
                        {props.author.name} <br /> {props.author.date}
                      </p>
                    </Col>
                    <Col>
                      <div className="divider dividerL"></div>
                    </Col>
                    <Col
                      className="shareML"
                      md={{ offset: 1, span: 4 }}
                      lg={12}
                    >
                      <ArticleCTA />
                    </Col>
                  </Row>
                </Col>
              ) : null}
            </Row>

            <div className="divider dividerTop"></div>

            <div className="contentTop">{props.children}</div>

            {!props.hangingQuotation ? null : (
              <div className="hangingQuotation">
                <p>{props.hangingQuotation.content}</p>

                <div className="author">
                  <div className="avatar">
                    <img src={props.hangingQuotation.avatar} alt="" />
                  </div>

                  <div className="authorInfo">
                    <p>
                      {props.hangingQuotation.author},{" "}
                      {props.hangingQuotation.authorTitle}
                    </p>
                    <p>{props.hangingQuotation.association}</p>
                  </div>
                </div>
              </div>
            )}

            <div className="contentBttm">
              {!props.contentBttm ? null : props.contentBttm}
            </div>

            {!props.imgInbetween ? null : (
              <div className="imgInbetween">
                <div className="imgContainer">
                  <img
                    src={props.imgInbetween.img}
                    alt={props.imgInbetween.caption}
                  />
                </div>
                <p>{props.imgInbetween.caption}</p>
              </div>
            )}

            {!props.subsection
              ? null
              : props.subsection.map((section, i) => {
                  return (
                    <div className="subsection">
                      <h3>{section.title}</h3>
                      <p>{section.content}</p>
                    </div>
                  );
                })}

            {props.signup ? (
              <div className="signup">
                <h2>Ready to get started?</h2>
                <button className="primaryBttn bttn">Sign up now</button>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Articles;
