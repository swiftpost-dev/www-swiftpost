import React from "react";

import ArticleCTA from "../templates/ArticleCTA";
import Articles from "../templates/Articles";
import ImgOverSection from "../templates/ImgOverSection";

import {AiOutlineArrowLeft} from "react-icons/ai"

function ArticleOne() {
  const sampleTags = ["Beverage", "Company news"];

  const authorInfo = {
    avatar: "/assets/articleOne/news-avatar-mikechalmers.jpg",
    name: "Mike Chalmer",
    date: "September 28, 2021",
  };

  const sampleContent = [
    <p>
      Short intro paragraph vivamus dapibus sollicitudin sollicitudin. Eleifend
      lobortis tortor, quis ornare metus mollis id. Aliquam erat volutpat.
      Quisque vehicula tellus et pellentesque iaculis. Curabitur tincidunt
      porttitor sem, sit amet varius mi elementum ac. Pellentesque sit amet
      convallis felis. Curabitur varius tortor.
    </p>,
    <p>
      Nunc ut sapien feugiat, euismod nulla eu, condimentum quam. Quisque odio
      justo, eleifend vitae augue nec, finibus viverra nunc. Fusce sagittis ut
      nisi quis molestie. Mauris nisl nibh, feugiat eget efficitur nec, mattis
      vitae libero. Suspendisse leo ipsum, pharetra non turpis mollis, ultrices
      blandit eros. Suspendisse potenti. Donec facilisis mauris erat, vitae
      vestibulum augue mattis nec. <br />
      <br />
      Sed euismod leo sapien, nec convallis augue interdum at. Vivamus maximus
      mauris in diam convallis tincidunt. Suspendisse vestibulum felis magna,
      nec viverra nisi dapibus vel. Duis maximus facilisis arcu, scelerisque
      pharetra nulla venenatis vel. Donec lorem libero, congue ac metus vel,
      lobortis aliquam lorem. Sed blandit metus vitae ipsum vehicula, vitae
      facilisis arcu condimentum. Morbi vulputate nec tellus sit amet finibus.
      Cras nec augue sed sapien porta faucibus eget sit amet dui. Ut sagittis
      nisi commodo, feugiat elit sit amet, venenatis metus.
    </p>,
  ];

  const sampleHangingQuotation = {
    avatar: "/assets/articleOne/news-avatar-mikechalmers.jpg",
    content:
      "Hanging quotation mark is optional but preferred for an extra level of professionalism and attention to detail. It can be achieved with negative text-indent on the blockquote first-child (note that “hanging-punctuation: first” doesn’t work in all browsers). Sed euismod leo sapien, nec convallis.”",
    author: "Liora Ipsum",
    authorTitle: "Executive Director",
    association: "Twenty Valley Tourism Association",
  };

  const sampleContentBttm = [
    <p>
      Nunc non facilisis erat. In hac habitasse platea dictumst. Etiam mauris
      quam, hendrerit et ultricies ac, eleifend eget turpis. Morbi non porta
      justo. Phasellus purus tortor, aliquam quis condimentum eget, dignissim et
      mauris. Mauris aliquet nisi sit amet est tempor vestibulum. Nunc est
      nulla, porttitor euismod mi imperdiet, porta mollis magna. Mauris
      imperdiet ligula dolor, sit amet egestas augue accumsan vel.
    </p>,
  ];

  const imgInbetween = {
    img: "/assets/articleOne/Inline_photo_any_size_fill_width.png",
    caption: (
      <p>
        Lorem ipsum photo caption style if required. Photos can be any height
        and would fill the column width. Mauris aliquet nisi sit amet est tempor
        vestibulum. Photo by{" "}
        <a href="#" className="link">
          Liora Ipsum
        </a>
        .
      </p>
    ),
  };

  const sampleSubsection = [
    {
      title: "A subhead style if required",
      content:
        "Cras egestas eleifend faucibus. Proin facilisis egestas pretium. Maecenas feugiat est nec consequat vestibulum. Vestibulum eget elit vitae turpis tincidunt sodales. Nulla risus ex, consectetur id ornare sit amet, ornare sed leo. Maecenas tempor magna a scelerisque imperdiet. Integer commodo nibh a blandit finibus. Nulla efficitur ex ut posuere consequat. Nunc blandit risus ac massa.",
    },
    {
      title:
        "Maecenas tempor magna a scelerisque imperdiet. Integer commodo nibh a blandit finibus.",
      content:
        "Phasellus semper velit a sem convallis auctor. Sed quam sem, venenatis sed tempor ac, pharetra eget magna. Integer ullamcorper condimentum est at malesuada. Morbi ultrices eu tellus eget rutrum. Praesent dignissim feugiat nulla sed commodo. Vivamus orci urna, vehicula id molestie at, mollis ut urna. Vestibulum sed odio nec magna hendrerit pellentesque sit amet id lorem.",
    },
  ];

  return (
    <div className="article">
      <Articles
        cover="/assets/articleOne/news_template.png"
        tags={sampleTags}
        title="Twenty Valley igula lorem est aliquam erat qui, eget lobortis metus
        dolor sollicitudin"
        author={authorInfo}
        // content={sampleContent}
        hangingQuotation={sampleHangingQuotation}
        contentBttm={sampleContentBttm}
        imgInbetween={imgInbetween}
        subsection={sampleSubsection}
        signup={true}
      >
        <div></div>
      </Articles>

      <div className="articleBttm">
        <div className="layoutContainer">
          <ArticleCTA />
          <div className="divider"></div>
          <a href="#" className="articleBack">
            <AiOutlineArrowLeft /> Back to article list
          </a>
        </div>
      </div>
      <ImgOverSection
        bgColor="transparent"
        img="/assets/articleOne/news-cta-getstarted@2x.png"
        title="Start shipping today."
        text="Experience next-level customer service and ship more for less with Swiftpost."
        button={{
          text: "Get started",
          destination: "https://app.swiftpost.com/signup",
          newWindow: true,
        }}
      />
    </div>
  );
}

export default ArticleOne;
