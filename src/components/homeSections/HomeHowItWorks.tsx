import React from "react";
import { Trans, useTranslation } from "react-i18next";
import { HashLink } from "react-router-hash-link";
import SmarthubCount from "../../api/countSmarthubs";
import { HowContent, Partner } from "../../types";

function HomeHeader() {
  const { t } = useTranslation();

  const scrollWithOffset = (el: any) => {
    const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
    const yOffset = -70;
    window.scrollTo({ top: yCoordinate + yOffset, behavior: "smooth" });
  };

  const howContent: Array<HowContent> = [
    {
      img: "/assets/main/home-howitworks1@2x.png",
      title: t("Home.Header.HowSwiftpostWorks.CreateShipment.title"),
      description: t(
        "Home.Header.HowSwiftpostWorks.CreateShipment.description"
      ),
    },
    {
      img: "/assets/main/home-howitworks2@2x.png",
      title: t("Home.Header.HowSwiftpostWorks.PickupDropoff.title"),
      description: (
        <Trans
          i18nKey="Home.Header.HowSwiftpostWorks.PickupDropoff.description"
          values={{ smarthubCount: SmarthubCount() }}
        />
      ),
      link: (
        <HashLink
          scroll={(el) => scrollWithOffset(el)}
          to="/#smarthub-locator"
          className="link"
        >
          {t("Home.Header.HowSwiftpostWorks.PickupDropoff.button")}
        </HashLink>
      ),
    },
    {
      img: "/assets/main/home-howitworks3@2x.png",
      title: t("Home.Header.HowSwiftpostWorks.TrackingAndStatus.title"),
      description: t(
        "Home.Header.HowSwiftpostWorks.TrackingAndStatus.description"
      ),
    },
  ];

  const partnerList: Array<Partner> = [
    {
      img: "/../assets/main/mckesson-canada-logo-vector.png",
      alt: t("Home.Header.CouriersPartnersCustomers.imageAlt.mckesson"),
    },
    {
      img: "/assets/main/home-partner-usps@2x.png",
      alt: t("Home.Header.CouriersPartnersCustomers.imageAlt.usps"),
    },
    {
      img: "/assets/main/logoroutesio_resized.png",
      alt: t("Home.Header.CouriersPartnersCustomers.imageAlt.routesIo"),
    },
    {
      img: "/assets/main/brother-logo-1-resized.png",
      alt: t("Home.Header.CouriersPartnersCustomers.imageAlt.brotherPrinters"),
    },
    {
      img: "/assets/main/creeksidecolorlogo-resized.png",
      alt: t("Home.Header.CouriersPartnersCustomers.imageAlt.creekside"),
    },
    {
      img: "/assets/main/routestransport-resized.png",
      alt: t("Home.Header.CouriersPartnersCustomers.imageAlt.routesTransport"),
    },
    {
      img: "/assets/main/home-partner-canpar@2x.png",
      alt: t("Home.Header.CouriersPartnersCustomers.imageAlt.canpar"),
    },
    {
      img: "/assets/main/home-partner-icscourier@2x.png",
      alt: t("Home.Header.CouriersPartnersCustomers.imageAlt.icscourier"),
    },
  ];

  return (
    <div className="headerBottom" id="how-it-works">
      <div className="layoutContainer">
        <div className="how">
          <div className="layoutContainer">
            <p className="title">
              {t("Home.Header.HowSwiftpostWorks.subtitle")}
            </p>
            <h3>{t("Home.Header.HowSwiftpostWorks.title")}</h3>
            <div className="howDetails columnsContainer">
              {howContent.map((section, i) => {
                return (
                  <div className="singleColumn three">
                    <div className="imgContainer">
                      <img src={section.img} alt="" />
                    </div>
                    <div className="text">
                      <h2>{i + 1}.</h2>
                      <h4>{section.title}</h4>
                      <p>{section.description}</p>
                      {section.link ?? null}
                    </div>
                  </div>
                );
              })}
              <div className="wind inbetween">
                <img
                  src={"/assets/main/home-howitworks1b@2x.png"}
                  alt={t("Home.Header.HowSwiftpostWorks.ImageAlt.wind")}
                />
              </div>

              <div className="dottedLine inbetween">
                <img
                  src={"/assets/main/home-howitworks2b@2x.png"}
                  alt={t("Home.Header.HowSwiftpostWorks.ImageAlt.dottedLine")}
                />
              </div>
            </div>
          </div>
          {/* closing for columns container */}
        </div>

        <div className="partners">
          <div className="layoutContainer">
            <p className="title">
              {t("Home.Header.CouriersPartnersCustomers.title")}
            </p>
            <div className="columnsContainer">
              {partnerList.map((partner, i) => {
                return (
                  <div className="partnerContainer" key={i}>
                    <img src={partner.img} alt={partner.alt} />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomeHeader;
