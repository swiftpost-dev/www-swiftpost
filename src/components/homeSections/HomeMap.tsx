import React from "react";
import { Trans, useTranslation } from "react-i18next";
import SmarthubCount from "../../api/countSmarthubs";
import Map from "../templates/Map";

function HomeMap() {
  const { t } = useTranslation();
  return (
    <div className="homeMap" id="smarthub-locator">
      <div className="layoutContainer">
        <div className="homeMapText">
          <div className="titleText">
            <p className="tag">
              <Trans
                i18nKey="Home.SmarthubLocator.tag"
                values={{ smarthubCount: SmarthubCount() }}
              />
            </p>
            <h3>
              <Trans i18nKey="Home.SmarthubLocator.title">
                Find a smart<span className="yellowText">hub</span>
              </Trans>
            </h3>
          </div>
          <p className="description">{t("Home.SmarthubLocator.description")}</p>
        </div>

        <Map />
      </div>
    </div>
  );
}
export default HomeMap;
