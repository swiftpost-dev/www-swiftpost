import request from "request";

const ContactSales = (data: any) =>
  new Promise((resolve, reject) => {
    try {
      request(
        {
          url: "https://www.swiftpost.com/contact",
          method: "POST",
          headers: {
            "content-type": "application/json",
          },
          body: data,
          json: true,
        },
        function (e, r, b) {
          if (e) {
            reject(false);
          } else {
            resolve(true);
          }
        }
      );
    } catch (e) {
      reject(e);
    }
  });

export default ContactSales;
