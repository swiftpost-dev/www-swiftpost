import React from "react";
import { Col, Row } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { AiOutlineCheckCircle, AiOutlineMenu } from "react-icons/ai";
import { IoLocationOutline } from "react-icons/io5";
import { HashLink } from "react-router-hash-link";
import beverageIcon from "../../assets/main/home-segment-beverage-icon.json";
import ecommerceIcon from "../../assets/main/home-segment-ecommerce-icon.json";
import healthcareIcon from "../../assets/main/home-segment-healthcare-icon.json";
import FloatingImg from "../templates/FloatingImg";
import LottieContainer from "../templates/LottieContainer";

function HomeSectors() {
  const { t } = useTranslation();

  const sectionsData = [
    {
      icon: ecommerceIcon,
      hashlink: "ecommerce#top",
      onTop: (
        <div className="floatingCard">
          <div className="topBar">
            <p>{t("Home.Sectors.Sections.Ecommerce.FloatingCard.title")}</p>
            <div className="cardIconContainer">
              <IoLocationOutline />
            </div>
          </div>

          <div className="cardContent">
            <p className="primaryText">
              {t("Home.Sectors.Sections.Ecommerce.FloatingCard.subtitle")}
            </p>
            <p>
              {t("Home.Sectors.Sections.Ecommerce.FloatingCard.description")}
            </p>
            <div className="cardTimeline">
              <div className="timelinecontainer">
                <div className="dot">
                  <img
                    src="/assets/main/timeline-check-with-blue-background.png"
                    alt={t(
                      "Home.Sectors.Sections.Ecommerce.FloatingCard.Timeline.imageAlt"
                    )}
                  />
                </div>
                <div className="content">
                  <p>
                    {t(
                      "Home.Sectors.Sections.Ecommerce.FloatingCard.Timeline.checkingAddress"
                    )}
                  </p>
                </div>
              </div>
              <div className="timelinecontainer">
                <div className="dot">
                  <img
                    src="/assets/main/timeline-check-with-blue-background.png"
                    alt={t(
                      "Home.Sectors.Sections.Ecommerce.FloatingCard.Timeline.imageAlt"
                    )}
                  />
                </div>
                <div className="content">
                  <p>
                    {t(
                      "Home.Sectors.Sections.Ecommerce.FloatingCard.Timeline.addressVerified"
                    )}
                  </p>
                </div>
              </div>
              <div className="timelinecontainer">
                <div className="dot">
                  <img
                    src="/assets/main/blue-solid-circle.png"
                    alt={t(
                      "Home.Sectors.Sections.Ecommerce.FloatingCard.Timeline.imageAlt"
                    )}
                  />
                </div>
                <div className="content">
                  <p>
                    {t(
                      "Home.Sectors.Sections.Ecommerce.FloatingCard.Timeline.processingShipment"
                    )}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      ),
      title: <h3>{t("Home.Sectors.Sections.Ecommerce.title")}</h3>,
      bttmImg: "/assets/main/home-segment-ecommerce-main.jpg",
      imgAlt: t("Home.Sectors.Sections.Ecommerce.imageAlt"),
      description: t("Home.Sectors.Sections.Ecommerce.description"),
    },
    {
      icon: healthcareIcon,
      hashlink: "healthcare#top",
      onTop: (
        <div className="floatingCard healthcare">
          <div className="topCard">
            <div className="topBar">
              <p>
                {t(
                  "Home.Sectors.Sections.Healthcare.FloatingCard.TopCard.title"
                )}
              </p>
            </div>
            <div className="topCardContent">
              <p className="grey">
                {t(
                  "Home.Sectors.Sections.Healthcare.FloatingCard.TopCard.subtitle"
                )}
              </p>
              <p>
                {t(
                  "Home.Sectors.Sections.Healthcare.FloatingCard.TopCard.status"
                )}
              </p>
              <p className="time">
                {t(
                  "Home.Sectors.Sections.Healthcare.FloatingCard.TopCard.time"
                )}
              </p>
            </div>
          </div>

          <div className="bottomCard">
            <div className="bottomCardText">
              <p className="status">
                {t(
                  "Home.Sectors.Sections.Healthcare.FloatingCard.BottomCard.status"
                )}
              </p>
              <p className="time">
                {t(
                  "Home.Sectors.Sections.Healthcare.FloatingCard.BottomCard.time"
                )}
              </p>
            </div>
            <div className="cardIconContainer">
              <AiOutlineCheckCircle />
            </div>
          </div>
        </div>
      ),
      title: <h3>{t("Home.Sectors.Sections.Healthcare.title")}</h3>,
      bttmImg: "/assets/main/home-segment-healthcare-main.jpg",
      imgAlt: t("Home.Sectors.Sections.Healthcare.imageAlt"),
      description: t("Home.Sectors.Sections.Healthcare.description"),
    },
    {
      icon: beverageIcon,
      hashlink: "beverage#top",
      onTop: (
        <div className="floatingCard beverage">
          <div className="beigeBg">
            <div className="cardIconContainer">
              <AiOutlineMenu />
            </div>
            <p>
              {t("Home.Sectors.Sections.Beverage.FloatingCard.TopHalf.title")}
            </p>
          </div>
          <div className="imgContainer">
            <img
              src="/assets/main/wine-bottle.png"
              alt={t("Home.Sectors.Sections.Beverage.FloatingCard.imageAlt")}
            />
          </div>
          <div className="ctaContainer">
            <p className="grey">
              {t(
                "Home.Sectors.Sections.Beverage.FloatingCard.BottomHalf.description"
              )}
            </p>
            <button className="bttn primaryBttn">
              {t(
                "Home.Sectors.Sections.Beverage.FloatingCard.BottomHalf.button"
              )}
            </button>
          </div>
        </div>
      ),
      title: <h3>{t("Home.Sectors.Sections.Beverage.title")}</h3>,
      bttmImg: "/assets/main/home-segment-beverage-main.jpg",
      imgAlt: t("Home.Sectors.Sections.Beverage.imageAlt"),
      description: t("Home.Sectors.Sections.Beverage.description"),
    },
  ];

  return (
    <div className="homeSectors">
      <div className="topText">
        <p className="title">{t("Home.Sectors.title")}</p>
        <h5>{t("Home.Sectors.subtitle")}</h5>
      </div>

      <div className="bttmSections">
        {sectionsData.map((section, i) => {
          return (
            <div
              key={`HomeSector_${i}`}
              className={i % 2 === 1 ? "singleSection odd" : "singleSection"}
            >
              <div className="layoutContainer">
                <div className="singleSectionContent">
                  <Row className="iconContainer">
                    <Col sm={{ offset: 1 }}>
                      {/* <Lottie options={section.icon} width={150} height={100} /> */}
                      <LottieContainer
                        icon={section.icon}
                        width={150}
                        height={100}
                      />
                    </Col>
                  </Row>

                  {section.title}

                  {i % 2 === 1 ? (
                    <FloatingImg imgBottom={section.bttmImg} position="right">
                      {section.onTop}
                    </FloatingImg>
                  ) : (
                    <FloatingImg imgBottom={section.bttmImg} position="left">
                      {section.onTop}
                    </FloatingImg>
                  )}

                  <p className="description">{section.description}</p>

                  <div className="buttonContainer">
                    <HashLink to={section.hashlink}>
                      <button className="bttn outlinedBttn">
                        {t("Home.Sectors.Sections.button")}
                      </button>
                    </HashLink>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default HomeSectors;
