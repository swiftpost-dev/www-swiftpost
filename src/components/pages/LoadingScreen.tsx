import React from "react";
import { RiLoader5Fill } from "react-icons/ri";
import "../../styles/_loading.scss";

function LoadingScreen() {
  return (
    <div className="loadingScreen">
      <RiLoader5Fill className="lds-roller" />
    </div>
  );
}
export default LoadingScreen;
