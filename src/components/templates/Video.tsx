import React from "react";
import { Modal } from "react-bootstrap";
import { useTranslation } from "react-i18next";

export default function Video() {
  const { t } = useTranslation();

  const [modalOpen, setModalOpen] = React.useState<boolean>(false);
  return (
    <>
      <a href="#" onClick={() => setModalOpen(!modalOpen)}>
        <span className="nav-link-inner--text">
          {t("Navigation.HowItWorks")}
        </span>
      </a>
      <Modal
        show={modalOpen}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        onHide={() => setModalOpen(!modalOpen)}
      >
        <Modal.Body style={{ padding: 0 }}>
          <iframe
            className="card"
            id="embed-video"
            style={{ width: "100%", height: "50vh" }}
            src="https://www.youtube.com/embed/bLi8ddyV-tI?modestbranding=1;controls=2;hd=1;showinfo=0;rel=0;fs=1;enablejsapi=1"
          />
        </Modal.Body>
      </Modal>
    </>
  );
}
