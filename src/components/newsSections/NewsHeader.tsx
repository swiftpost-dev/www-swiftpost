import React from "react";
import Lottie from "react-lottie";
import { Row, Col } from "react-bootstrap";
import { useTranslation } from "react-i18next";

import headerImg from "../../assets/news/news-landing-bg.json";

function NewsHeader() {
  const { t } = useTranslation();

  return (
    <div className="layoutContainer">
      <Row className="newsHeader">
        <Col className="text" md={5} lg={7}>
          <p className="smallTitle">{t("News.title")}</p>
          <h1>{t("News.subtitle")}</h1>
        </Col>
        <Col className="imgContainer" md={5} lg={5}>
          <Lottie
            options={{
              loop: true,
              autoplay: true,
              animationData: headerImg,
              rendererSettings: {
                preserveAspectRatio: "xMinYMid slice",
              },
            }}
            height={"100%"}
          />
        </Col>
      </Row>
    </div>
  );
}
export default NewsHeader;
