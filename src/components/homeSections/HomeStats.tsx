import React, { useState } from "react";
import Lottie from "react-lottie";
import { useTranslation, Trans } from "react-i18next";

import Stats from "../templates/Stats";

import homeStatsBg from "../../assets/main/home-stats-bg.json";

function HomeStats() {
  const { t } = useTranslation();
  const statsData = [
    {
      num: (
        <Trans
          i18nKey="Home.Stats.StatsData.Data.num"
          values={{ statsNum: "274" }}
        />
      ),
      description: t("Home.Stats.StatsData.Description.num"),
    },
    {
      num: (
        <Trans
          i18nKey="Home.Stats.StatsData.Data.percentage"
          values={{ statsPercentage: "100+" }}
        />
      ),
      description: t("Home.Stats.StatsData.Description.percentage"),
    },
  ];
  return (
    <div className="homeStats">
      <div className="homeStatsBg">
        <Lottie
          options={{
            loop: true,
            autoplay: true,
            animationData: homeStatsBg,
            rendererSettings: {
              preserveAspectRatio: "xMidYMid slice",
            },
          }}
          height={"100%"}
        />
      </div>
      <div className="imgS">
        <img src="/assets/main/home-delivery-small.jpg" alt={t("Home.Stats.imageAlt")} />
      </div>

      <div className="imgML">
        <img src="/assets/main/home-delivery-largemedium.jpg" alt={t("Home.Stats.imageAlt")} />
      </div>

      <div className="layoutContainer">
        <div className="titleText">
          <p className="title">{t("Home.Stats.title")}</p>
          <h5>{t("Home.Stats.subtitle")}</h5>

          <Stats statsData={statsData} />
        </div>
      </div>
    </div>
  );
}
export default HomeStats;
