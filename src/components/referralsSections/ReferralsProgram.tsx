import React from "react";
import { Row, Col } from "react-bootstrap";
import { useTranslation } from "react-i18next";

function ReferralsProgram() {
  const { t } = useTranslation();
  return (
    <div className="referralsProgram">
      <div className="layoutContainer">
        <Row>
          <Col className="text" sm={12} md={11} lg={7}>
            <p className="smallTitle">{t("Referral.Program.title")}</p>
            <h4>{t("Referral.Program.subtitle")}</h4>
            <p>{t("Referral.Program.description")}</p>
          </Col>
          <Col
            className="imgContainer"
            sm={12}
            md={{ span: 10, offset: 2 }}
            lg={{ span: 7, offset: 5 }}
          >
            <img src="/assets/referrals/referrals-coinstack@2x.png" alt={t("Referral.Program.imageAlt")} />
          </Col>
        </Row>
      </div>
    </div>
  );
}
export default ReferralsProgram;
