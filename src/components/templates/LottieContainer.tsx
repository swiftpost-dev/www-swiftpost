import React from "react";
import { propTypes } from "react-bootstrap/esm/Image";
import Lottie from "react-lottie";
import useIsInViewport from "use-is-in-viewport";

interface LottieContainerProps {
  icon: any;
  width: string | number;
  height: string | number;
}

function LottieContainer(props: LottieContainerProps) {
  const [isInViewport, targetRef] = useIsInViewport();
  const [isStopped, setIsStopped] = React.useState(true);

  React.useEffect(() => {
    setIsStopped(!isInViewport);
  }, [isInViewport]);

  return (
    <div className="LottieContainer" ref={targetRef}>
      <Lottie
        options={{ loop: false, autoplay: false, animationData: props.icon }}
        width={props.width}
        height={props.height}
        isStopped={isStopped}
      />
    </div>
  );
}
export default LottieContainer;
