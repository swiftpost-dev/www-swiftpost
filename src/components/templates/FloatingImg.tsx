import React from "react";
import SlideAnimate from "./SlideAnimate";

interface FloatingImgInterface {
  imgBottom: string;
  imgAlt?: string;
  position: string;
  children: React.ReactNode;
}

function FloatingImg(props: FloatingImgInterface) {
  return (
    <div
      className={
        props.position === "left"
          ? "floatingContainer left"
          : "floatingContainer right"
      }
    >
      <SlideAnimate>
        <div className="onBottom floatingSub">
          <img src={props.imgBottom} alt={props.imgAlt} />
        </div>
        <div className="onTop floatingSub">{props.children}</div>
      </SlideAnimate>
    </div>
  );
}

export default FloatingImg;
