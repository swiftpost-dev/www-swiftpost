import React from "react";
import { Trans, useTranslation } from "react-i18next";
import { HashLink } from "react-router-hash-link";
import LottieContainer from "../templates/LottieContainer";
import FloatingImg from "./FloatingImg";

interface SegmentCurvedBgInterface {
  reduceImg: string;
  reduceImgAlt: string;
  reduceCard: object;
  reduceTitle: string;
  reduceContent: string;
  simpleImg: string;
  simpleImgAlt: string;
  simpleCard: object;
  simpleTitle: string;
  simpleContent: string;
  saveContent: string;
  contactImg: string;
  contactImgAlt: string;
}

function SegmentFourSections(props: SegmentCurvedBgInterface) {
  const { t } = useTranslation();
  return (
    <div className="segmentFourSectionsContainer">
      <div className="layoutContainer">
        <div className="sectionContainer reduceSection">
          <p className="title">{t("SegmentsCommon.FourSections.reduce")}</p>
          <div className="segmentFloatingContainer">
            <FloatingImg
              imgBottom={props.reduceImg}
              position="right"
              imgAlt={props.reduceImgAlt}
            >
              <div>
                <LottieContainer
                  icon={props.reduceCard}
                  width={"100%"}
                  height={"100%"}
                />
                {/* <Lottie
                  options={{
                    loop: true,
                    autoplay: true,
                    animationData: props.reduceCard,
                  }}
                  width={"100%"}
                  height={"100%"}
                /> */}
              </div>
            </FloatingImg>
          </div>
          <div className="textContainer">
            <p className="subtitle">{props.reduceTitle}</p>
            <p className="content">{props.reduceContent}</p>
          </div>
          <div className="dividerContainer"></div>
        </div>

        <div className="sectionContainer simpleSection">
          <p className="title">{t("SegmentsCommon.FourSections.simple")}</p>
          <FloatingImg
            imgBottom={props.simpleImg}
            position="right"
            imgAlt={props.simpleImgAlt}
          >
            <div>
              <LottieContainer
                icon={props.simpleCard}
                width={"100%"}
                height={"100%"}
              />
              {/* <Lottie
                options={{
                  loop: true,
                  autoplay: true,
                  animationData: props.simpleCard,
                }}
                width={"100%"}
                height={"100%"} */}
              {/* /> */}
            </div>
          </FloatingImg>
          <div className="textContainer">
            <p className="subtitle">{props.simpleTitle}</p>
            <p className="content">{props.simpleContent}</p>
          </div>
          <div className="dividerContainer"></div>
        </div>

        <div className="sectionContainer saveContainer">
          <div className="textContainer">
            <p className="subtitle saveSubtitle">
              {t("SegmentsCommon.FourSections.save")}
            </p>
            <ul>
              {props.saveContent.split("; ").map((item) => {
                return (
                  <li>
                    <span>{item}</span>
                  </li>
                );
              })}
            </ul>
            <HashLink to="contact#top">
              <button className="primaryBttn bttn">
                {t("SegmentsCommon.FourSections.button")}
              </button>
            </HashLink>
          </div>
          <div className="dividerContainer"></div>
        </div>

        <div className="sectionContainer contact">
          <div className="contactInner">
            <div className="imgContainer">
              <img src={props.contactImg} alt={props.contactImgAlt} />
            </div>
            <p>
              <Trans i18nKey="SegmentsCommon.FourSections.contact">
                <HashLink to="contact#top" className="link">
                  Have any questions?
                </HashLink>
                Email us at
                <a href="mailto: sales@swiftpost.com">sales@swiftpost.com</a>,
                or talk to a sale representative at
                <a href="tel:18886556258">1-888-655-6258</a>.
              </Trans>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SegmentFourSections;
