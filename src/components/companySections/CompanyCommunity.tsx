import React from "react";
import { Col, Row } from "react-bootstrap";
import { Trans, useTranslation } from "react-i18next";
import { HashLink } from "react-router-hash-link";

function CompanyCommunity() {
  const { t } = useTranslation();
  const eventList = ["OCW", "I4C", "WineAlign", "EtsyOttawa", "CheerfullyMade"];

  const eventList2 = [
    "EtsyStreet",
    "ModernMakersMarket",
    "OttawaArtisans",
    "KingstonMadeCo",
    "HelloHappy",
  ];
  return (
    <div className="companyCommunity">
      <div className="layoutContainer">
        <Row>
          <Col sm={12} md={12} lg={7} className="largeTitle">
            <p>{t("Company.Community.title")}</p>
          </Col>
          <Col sm={12} md={6}>
            <div className="text">
              <p>
                <Trans i18nKey="Company.Community.description">
                  We're passionate about helping entrepreneurs grow their
                  business doing what they love. Whether you're an Etsy maker or
                  you have thriving Shopify Store, our
                  <HashLink className="link" to="ecommerce#top">
                    ecommerce shipping solutions
                  </HashLink>
                  provide a reliable way to ship more for less.
                </Trans>
              </p>

              <div className="listContainer">
                <p className="smallTitle">
                  {t("Company.Community.List.title")}
                </p>
                <Row>
                  <Col sm={12} lg={6}>
                    <ul className="topul">
                      {eventList.map((item) => {
                        return (
                          <li>
                            <a
                              target="_blank"
                              rel="noreferrer"
                              href={t(
                                `Company.Community.List.Description.${item}.link`
                              )}
                              className="link"
                            >
                              {t(
                                `Company.Community.List.Description.${item}.name`
                              )}
                            </a>
                          </li>
                        );
                      })}
                    </ul>
                  </Col>

                  <Col sm={12} lg={6}>
                    <ul className="bttnul">
                      {eventList2.map((item) => {
                        return (
                          <li>
                            <a
                              target="_blank"
                              rel="noreferrer"
                              href={t(
                                `Company.Community.List.Description.${item}.link`
                              )}
                              className="link"
                            >
                              {t(
                                `Company.Community.List.Description.${item}.name`
                              )}
                            </a>
                          </li>
                        );
                      })}
                    </ul>
                  </Col>
                </Row>
              </div>
            </div>
          </Col>
          <Col sm={12} md={{ span: 5, offset: 1 }} lg={{ span: 4, offset: 1 }}>
            <div className="imgContainer">
              <img
                src="/assets/company/company-community-full.png"
                alt={t("Company.Community.imgAlt")}
              />
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}
export default CompanyCommunity;
