import * as L from "leaflet";
import { LatLngTuple } from "leaflet";
import React from "react";
import { Button, InputGroup, Spinner } from "react-bootstrap";
import { BiCurrentLocation } from "react-icons/bi";
import { IoNavigate } from "react-icons/io5";
import { RiFullscreenExitFill, RiFullscreenFill } from "react-icons/ri";
import {
  MapContainer,
  Marker,
  Popup,
  TileLayer,
  useMap,
  ZoomControl,
} from "react-leaflet";
import MarkerClusterGroup from "react-leaflet-cluster";
import ListSmarthubs from "../../api/listSmarthubs";
import { Geocoords, Smarthub } from "../../types";

function ChangeView({ center, zoom }: any) {
  const map = useMap();
  map.setView(center, zoom);
  return null;
}

function getDistanceFromLatLonInKm(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number
) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return Math.ceil(d);
}

function deg2rad(deg: number) {
  return deg * (Math.PI / 180);
}

function Map() {
  const [position, setPosition] = React.useState<LatLngTuple>([
    43.5061345,
    -79.6810717,
  ]);
  const [isLocating, setIsLocating] = React.useState<boolean>(false);
  const [myLocation, setMyLocation] = React.useState<LatLngTuple | null>(null);
  const [zoom, setZoom] = React.useState<number>(7);
  const smarthubs: Array<Smarthub> = ListSmarthubs();
  const [fullscreen, setFullscreen] = React.useState<boolean>(false);

  var MyLocationIcon = new L.Icon({
    iconUrl:
      "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png",
    shadowUrl:
      "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
  });

  var SmarthubIcon = new L.Icon({
    iconUrl: "../../assets/map/smarthub-icon.svg",
    shadowUrl:
      "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
    iconSize: [25, 35],
    iconAnchor: [12, 35],
    popupAnchor: [1, -34],
    shadowSize: [35, 35],
  });

  const FullscreenToggle = () => {
    const map = useMap();

    const toggleFullscreen = () => {
      setFullscreen(!fullscreen);
      setTimeout(() => map.invalidateSize(), 100);
    };

    return (
      <div
        className="fullscreenControl"
        aria-label="Fullscreen"
        onClick={() => toggleFullscreen()}
      >
        {fullscreen ? <RiFullscreenExitFill /> : <RiFullscreenFill />}
      </div>
    );
  };

  const SmarthubList = (props: any) => {
    return (
      <div className="smarthubList">
        {props.items.map((smarthub: Smarthub) => (
          <></>
        ))}
      </div>
    );
  };

  const geolocate = () => {
    if (navigator.geolocation) {
      setIsLocating(true);
      navigator.geolocation.getCurrentPosition(
        (position: Geocoords) => {
          setIsLocating(false);
          setMyLocation([position.coords.latitude, position.coords.longitude]);
          setPosition([position.coords.latitude, position.coords.longitude]);
          setZoom(12);
        },
        (error) => {
          setIsLocating(false);
          if (error.code == error.PERMISSION_DENIED) {
            alert(
              "You have blocked this website from being able to find your location."
            );
          }
        }
      );
    } else {
      alert("Location finding is not allowed on your device");
      setIsLocating(false);
    }
  };

  return (
    <div className={`map ${fullscreen ? `fullscreen` : ""}`}>
      <MapContainer
        handle
        center={position}
        zoom={zoom}
        scrollWheelZoom={fullscreen}
        zoomControl={false}
        className={`mapWrapper`}
      >
        {/* <UserPanHandler /> */}
        <ChangeView center={position} zoom={zoom} />
        <TileLayer url="https://{s}.basemaps.cartocdn.com/rastertiles/voyager_labels_under/{z}/{x}/{y}{r}.png" />
        <ZoomControl position="bottomright" />
        {myLocation ? (
          <Marker
            key={"MyLocation"}
            position={myLocation}
            icon={MyLocationIcon}
          >
            <Popup>Your location</Popup>
          </Marker>
        ) : (
          <></>
        )}
        <MarkerClusterGroup chunkedLoading>
          {smarthubs.map((smarthub: Smarthub, count: number) => (
            <Marker
              key={`SmarthubLocation_${count}`}
              position={[smarthub.lat, smarthub.lng]}
              icon={SmarthubIcon}
            >
              <Popup>
                {myLocation ? (
                  <div className="tag">
                    <IoNavigate />
                    <p>
                      {getDistanceFromLatLonInKm(
                        smarthub.lat,
                        smarthub.lng,
                        myLocation[0],
                        myLocation[1]
                      )}
                      KM AWAY
                    </p>
                  </div>
                ) : (
                  ""
                )}
                <strong>{smarthub.name}</strong>
                <br />
                {smarthub.address1}
                <br />
                {smarthub.city}, {smarthub.state} {smarthub.postal}
                <p>
                  <a
                    href={`https://www.google.ca/maps/dir/${
                      myLocation ? `${myLocation[0]}+${myLocation[1]}/` : ``
                    }${smarthub.lat}+${smarthub.lng}`}
                    className="link"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Get Directions
                  </a>
                </p>
              </Popup>
            </Marker>
          ))}
        </MarkerClusterGroup>
        <FullscreenToggle />
        <InputGroup>
          <Button
            disabled={isLocating ? true : false}
            onClick={() => {
              if (myLocation) {
                setZoom(0);
                setTimeout(() => setZoom(15), 10);
              } else {
                geolocate();
              }
            }}
          >
            {isLocating ? (
              <>
                Finding...{" "}
                <Spinner size="sm" animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              </>
            ) : (
              <>
                {myLocation ? "" : <>Find my Location&nbsp;&nbsp;</>}
                <BiCurrentLocation />
              </>
            )}
          </Button>
        </InputGroup>
      </MapContainer>
    </div>
  );
}

export default Map;
