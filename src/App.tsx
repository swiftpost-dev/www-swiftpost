import React from "react";
import { useTranslation } from "react-i18next";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.scss";
import ArticleOne from "./components/pages/ArticleOne";
import Company from "./components/pages/Company";
import Contact from "./components/pages/Contact";
import Covid19 from "./components/pages/Covid19";
import Home from "./components/pages/Home";
import PrivacyPolicy from "./components/pages/PrivacyPolicy";
import Referrals from "./components/pages/Referrals";
import SegmentBeverage from "./components/pages/SegmentBeverage";
import SegmentEcommerce from "./components/pages/SegmentEcommerce";
import SegmentHealthcare from "./components/pages/SegmentHealthcare";
import Terms from "./components/pages/Terms";
import Tracking from "./components/pages/Tracking";
import Banner from "./components/templates/Banner";
import Footer from "./components/templates/Footer";
import Nav from "./components/templates/Nav";

import News from "./components/pages/News"

function App() {
  const { i18n } = useTranslation();
  const supportedLanguages = ["en-US", "en-CA", "fr-CA"];
  return (
    <div className="app">
      <Router>
        <Nav />
        <div style={{ height: "70px" }} />
        <Banner />
        <Switch>
          <Route exact path={`/terms`}>
            <Terms />
          </Route>
          <Route exact path={`/privacy-policy`}>
            <PrivacyPolicy />
          </Route>
          <Route exact path={`/covid-19`}>
            <Covid19 />
          </Route>
          <Route exact path={`/referrals`}>
            <Referrals />
          </Route>
          <Route exact path={`/beverage`}>
            <SegmentBeverage />
          </Route>
          <Route exact path={`/healthcare`}>
            <SegmentHealthcare />
          </Route>
          <Route exact path={`/ecommerce`}>
            <SegmentEcommerce />
          </Route>
          <Route exact path={`/referrals`}>
            <Referrals />
          </Route>
          <Route exact path={`/tracking`}>
            <Tracking />
          </Route>
          <Route exact path={`/news`}>
            <ArticleOne />
          </Route>
          <Route exact path={`/contact`}>
            <Contact />
          </Route>
          <Route exact path={`/about-us`}>
            <Company />
          </Route>
          <Route exact path={`/articles`}>
            <News />
          </Route>
          <Route path={`/`}>
            <Home />
          </Route>
          {/* <Route path={`/`}>
            <Redirect
              to={`/${
                supportedLanguages.indexOf(i18n.language) > -1
                  ? i18n.language
                  : "en-CA"
              }/`}
            />
          </Route> */}
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
