import React from "react";
import { Trans, useTranslation } from "react-i18next";
import { AiFillCheckCircle } from "react-icons/ai";
import Lottie from "react-lottie";
import landingBgL from "../../assets/main/home-landing-bg-large.json";
import landingBgM from "../../assets/main/home-landing-bg-medium.json";
import HeaderSideImg from "../templates/HeaderSideImg";

function HomeHeader() {
  const { t } = useTranslation();

  const circumference = ((2 * 22) / 7) * 16;
  const deliveredNum = circumference * 0.67;
  const transitNum = circumference * 0.3;
  const pickupNum = circumference * (1 - 0.67 - 0.3);

  return (
    <div className="headerWCreamBg">
      <div className="landingBgM">
        <Lottie
          options={{
            loop: true,
            autoplay: true,
            animationData: landingBgM,
          }}
          width={"100%"}
          height={"100%"}
        />
      </div>
      <div className="landingBgL">
        <Lottie
          options={{
            loop: true,
            autoplay: true,
            animationData: landingBgL,
          }}
          width={"100%"}
          height={"100%"}
        />
      </div>
      <div className="layoutContainer">
        <HeaderSideImg
          img={"/assets/main/home-landing-videoplaceholder-small.jpg"}
          video={"/assets/main/home-landing-video-small.mp4"}
          imgml={"/assets/main/home-landing-videoplaceholder-largemedium.jpg"}
          videoml={"/assets/main/home-landing-video-largemedium.mp4"}
          titleText={<h2 className="title">{t("Home.Header.Hero.title")}</h2>}
          secondaryText={t("Home.Header.Hero.subtitle")}
          buttonText={t("Home.Header.Hero.button")}
        >
          <div className="landingOverlay">
            <div className="topHalf">
              <p>{t("Home.Header.Hero.FloatingCard.title")}</p>
              <p>
                <Trans
                  i18nKey="Home.Header.Hero.FloatingCard.Delivered.num"
                  values={{ deliveredNum: 228419 }}
                />
              </p>
              <div className="status">
                <div className="iconContainer">
                  <AiFillCheckCircle color="#00ADDB" />
                </div>
                <p>
                  {t("Home.Header.Hero.FloatingCard.Delivered.description")}
                </p>
              </div>
            </div>
            <div className="imgContainer">
              <svg viewBox="0 0 32 32">
                <g strokeWidth="15" className="circles">
                  {/* grey */}
                  <circle
                    key={1}
                    cx="16"
                    cy="16"
                    r="16"
                    strokeDasharray={pickupNum + "," + circumference}
                    strokeDashoffset={"-" + deliveredNum}
                    stroke="#9BA5B9"
                  ></circle>
                  {/* dark blue  */}
                  <circle
                    key={2}
                    cx="16"
                    cy="16"
                    r="16"
                    strokeDasharray={transitNum + "," + circumference}
                    strokeDashoffset={"-" + (pickupNum + deliveredNum)}
                    stroke="#374B74"
                  ></circle>
                  {/* primary */}
                  <circle
                    key={3}
                    cx="16"
                    cy="16"
                    r="16"
                    strokeDasharray={deliveredNum + "," + circumference}
                    strokeDashoffset="0"
                    stroke="#00ADDB"
                  ></circle>
                </g>
              </svg>
              {/* <HomePieChart /> */}
            </div>
            <div className="bottomHalf">
              <div>
                <p>
                  <Trans
                    i18nKey="Home.Header.Hero.FloatingCard.InTransit.num"
                    values={{ transitNum: 12823 }}
                  />
                </p>
                <div className="status">
                  <p className="tr">
                    {t("Home.Header.Hero.FloatingCard.InTransit.description")}
                  </p>
                </div>
              </div>
              <div>
                <p>
                  <Trans
                    i18nKey="Home.Header.Hero.FloatingCard.Pickup.num"
                    values={{ pickupNum: 1106 }}
                  />
                </p>
                <div className="status">
                  <p className="ps">
                    {t("Home.Header.Hero.FloatingCard.Pickup.description")}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </HeaderSideImg>
      </div>
    </div>
  );
}

export default HomeHeader;
