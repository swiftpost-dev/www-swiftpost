import React from "react";
import NewsHeader from "../newsSections/NewsHeader";
import NewsHeadline from "../newsSections/NewsHeadline";
import NewsMain from "../newsSections/NewsMain";
import NewsSort from "../newsSections/NewsSort";
import ImgOverSection from "../templates/ImgOverSection";
import { useTranslation } from "react-i18next";

interface NewsInterface {
  img: string;
  tags: Array<string>;
  title: string;
}

function News() {
  const { t } = useTranslation();

  const articleList: Array<NewsInterface> = [
    {
      img: "/assets/news/news-example1.jpg",
      tags: ["Healthcare", "company news"],
      title:
        "Lorem ipsum dolor sit Swiftpost partners with McKesson Canada for aenean nec iaculis",
    },
    {
      img: "/assets/news/news_template.png",
      tags: ["Beverage", "company news"],
      title:
        "Twenty Valley igula lorem est aliquam erat qui, eget lobortis metus dolor sollicitudin",
    },
    {
      img: "/assets/news/news_template.png",
      tags: ["Ecommerce", "company news"],
      title: "Suspendisse vitae cursus the US tortor, in lobortis diam",
    },
    {
      img: "/assets/news/news_template.png",
      tags: ["Beverage"],
      title:
        "Beverage article with no photo, uses a placeholder img if required",
    },
  ];

  return (
    <div className="news">
      <NewsHeader />
      <NewsHeadline 
        img={articleList[0]["img"]}
        tags={articleList[0]["tags"]}
        title={articleList[0]["title"]}
      />
      <NewsSort />
      <NewsMain articleList={articleList} />

      <ImgOverSection
        bgColor="transparent"
        img="/assets/news/news-cta-getstarted@2x.png"
        title={t("News.CTA.title")}
        text={t("News.CTA.description")}
        button={{
          text: t("News.CTA.button"),
          destination: "https://app.swiftpost.com/signup",
          newWindow: true,
        }}
      />
    </div>
  );
}

export default News;
