import React from "react";

interface Bttn {
  text: string;
  destination: string;
  newWindow: boolean;
}

interface ImgOverSectionInterface {
  bgColor: string;
  img: string;
  title: string;
  text: string;
  button?: Bttn;
}

function ImgOverSection(props: ImgOverSectionInterface) {
  return (
    <div style={{ background: props.bgColor }}>
      <div className="layoutContainer">
        <div className="imgOverSection">
          <div className="imgContainer">
            <img src={props.img} alt="" />
          </div>

          <p className="title">{props.title}</p>

          <p>{props.text}</p>

          {props.button ? (
            <a
              href={props.button.destination}
              target={props.button.newWindow ? `_blank` : `_self`}
              rel="noreferrer"
            >
              <button className="outlinedBttn bttn">{props.button.text}</button>
            </a>
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
}

export default ImgOverSection;
